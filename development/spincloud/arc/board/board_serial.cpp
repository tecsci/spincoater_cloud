
/********************** inclusions *******************************************/

#include "board_serial.hpp"
#include "board_log.hpp"
#include "Arduino.h"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

// TODO: CODIGO REPETIDO
static void setup_(int serial_id) {

    switch (serial_id) {
    case BOARD_LAYOUT_SERIAL_0_PERIPH_ID:
        Serial.begin(BOARD_LAYOUT_SERIAL_0_BAUDRATE, BOARD_LAYOUT_SERIAL_0_CONFIG_MODE,BOARD_LAYOUT_SERIAL_0_RX, BOARD_LAYOUT_SERIAL_0_TX );
        return;
        break;
    case BOARD_LAYOUT_SERIAL_1_PERIPH_ID:
        Serial1.begin(BOARD_LAYOUT_SERIAL_1_BAUDRATE, BOARD_LAYOUT_SERIAL_1_CONFIG_MODE, BOARD_LAYOUT_SERIAL_1_RX, BOARD_LAYOUT_SERIAL_1_TX);
        return;
        break;
    case BOARD_LAYOUT_SERIAL_2_PERIPH_ID:
        Serial2.begin(BOARD_LAYOUT_SERIAL_2_BAUDRATE, BOARD_LAYOUT_SERIAL_2_CONFIG_MODE, BOARD_LAYOUT_SERIAL_2_RX, BOARD_LAYOUT_SERIAL_2_TX);
        return;
        break;
    default:
        break;
    }

}

/********************** external functions definition ************************/

uint32_t board_serial_write(int serial_id, void* p_buffer, uint32_t size)
{
    switch (serial_id) {
    case BOARD_LAYOUT_SERIAL_0_PERIPH_ID:
        return Serial.write((const uint8_t *) p_buffer, size);
        //return uart_write_bytes(UART_NUM_0, (char *) buffer, len );
        break;
    case BOARD_LAYOUT_SERIAL_1_PERIPH_ID:
        return Serial1.write((const uint8_t *) p_buffer, size);
        //return uart_write_bytes(UART_NUM_1, (char *) buffer, len );
        break;
    case BOARD_LAYOUT_SERIAL_2_PERIPH_ID:
        return Serial2.write((const uint8_t *) p_buffer, size);
        //return uart_write_bytes(UART_NUM_2, (char *) buffer, len );
        break;
    default:
        return 0;
        break;
    }

    return 0;
}

uint32_t board_serial_read(int serial_id, void* p_buffer, uint32_t size_max)
{
    switch (serial_id) {
    case BOARD_LAYOUT_SERIAL_0_PERIPH_ID:
        return Serial.readBytes((char *)p_buffer, size_max);
        //return uart_read_bytes(UART_NUM_0, (uint8_t*) buffer, lenmax , portMAX_DELAY );
        break;
    case BOARD_LAYOUT_SERIAL_1_PERIPH_ID:
        return Serial1.readBytes((char *)p_buffer, size_max);
        //return uart_read_bytes(UART_NUM_1, (uint8_t*) buffer, lenmax , portMAX_DELAY);
        break;
    case BOARD_LAYOUT_SERIAL_2_PERIPH_ID:
        return Serial2.readBytes((char *)p_buffer, size_max);
        //return uart_read_bytes(UART_NUM_2, (uint8_t*) buffer, lenmax , portMAX_DELAY);
        break;
    default:
        return 0;
        break;
    }

    return 0;

}

void board_serial_init(void)
{

#if 1 == BOARD_LAYOUT_SERIAL_0_ENABLE
    setup_(BOARD_LAYOUT_SERIAL_0_PERIPH_ID);
#endif

#if 1 == BOARD_LAYOUT_SERIAL_1_ENABLE
    setup_(BOARD_SERIAL_ID_UART_1);
#endif

#if 1 == BOARD_LAYOUT_SERIAL_2_ENABLE
    setup_(BOARD_SERIAL_ID_UART_2);
#endif

    BOARD_LOG_TRACE_FUNCTION();

}

void board_serial_deinit(void)
{
}

void board_serial_loop(void)
{
}

#if 0

void board_serial_init(){
    board_serial_setup(BOARD_UART_0); //console
    BOARD_LOG("UART 0 enabled");

#if 0 //UART 1 and UART 2 NOT in use
    board_serial_setup(BOARD_UART_1);
    board_serial_setup(BOARD_UART_2);
#endif

}
void board_serial_deinit(){

}
void board_serial_loop(){

}

void board_serial_setup(board_serial_id_t uart_id) {

	switch (uart_id) {
	case BOARD_UART_0:
		Serial.begin(BOARD_BAUD_RATE);
		return;
		break;
	case BOARD_UART_1:
		Serial1.begin(BOARD_BAUD_RATE, SERIAL_8N1, BOARD_RX1, BOARD_TX1);
		return;
		break;
	case BOARD_UART_2:
		Serial2.begin(BOARD_BAUD_RATE, SERIAL_8N1, BOARD_RX2, BOARD_TX2);
		return;
		break;
	default:
		break;
	}

}

int32_t board_serial_write( board_serial_id_t uart_id, void* buffer, uint32_t len ){

	switch (uart_id) {
	case BOARD_UART_0:
		return Serial.write((uint8_t *) buffer, len);
		//return uart_write_bytes(UART_NUM_0, (char *) buffer, len );
		break;
	case BOARD_UART_1:
		return Serial1.write((uint8_t *) buffer, len);
		//return uart_write_bytes(UART_NUM_1, (char *) buffer, len );
		break;
	case BOARD_UART_2:
		return Serial2.write((uint8_t *) buffer, len);
		//return uart_write_bytes(UART_NUM_2, (char *) buffer, len );
		break;
	default:
		return -1;
		break;
	}

	return -1;

}
int32_t board_serial_read( board_serial_id_t uart_id, void* buffer, uint32_t lenmax ){

	switch (uart_id) {
	case BOARD_UART_0:
		return Serial.readBytes((char *)buffer, lenmax);
		//return uart_read_bytes(UART_NUM_0, (uint8_t*) buffer, lenmax , portMAX_DELAY );
		break;
	case BOARD_UART_1:
		return Serial1.readBytes((char *)buffer, lenmax);
		//return uart_read_bytes(UART_NUM_1, (uint8_t*) buffer, lenmax , portMAX_DELAY);
		break;
	case BOARD_UART_2:
		return Serial2.readBytes((char *)buffer, lenmax);
		//return uart_read_bytes(UART_NUM_2, (uint8_t*) buffer, lenmax , portMAX_DELAY);
		break;
	default:
		return -1;
		break;
	}

	return -1;

}

void board_serial_swrite( board_serial_id_t uart_id, const char* buffer){

	switch (uart_id) {
	case BOARD_UART_0:
		Serial.printf("%s", buffer);
		break;
	case BOARD_UART_1:
		Serial1.printf("%s", buffer);
		break;
	case BOARD_UART_2:
		Serial2.printf("%s", buffer);
		break;
	default:
		break;
	}

	return;

}

void board_serial_int_write( board_serial_id_t uart_id, int i){

	switch (uart_id) {
	case BOARD_UART_0:
		Serial.printf("%d", i);
		break;
	case BOARD_UART_1:
		Serial1.printf("%d", i);
		break;
	case BOARD_UART_2:
		Serial2.printf("%d", i);
		break;
	default:
		break;
	}

	return;

}

void board_serial_sread( board_serial_id_t uart_id, char* buffer){

	switch (uart_id) {
	case BOARD_UART_0:
			if (Serial.available()) {
				strcpy(buffer, Serial.readStringUntil('\n').c_str());
			}
		break;
	case BOARD_UART_1:
		if (Serial1.available()) {
			strcpy(buffer, Serial1.readStringUntil('\n').c_str());
		}
		break;
	case BOARD_UART_2:
		if (Serial2.available()) {
			strcpy(buffer, Serial2.readStringUntil('\n').c_str());
		}
		break;
	default:
		break;
	}

	return;

}
#endif

/********************** end of file ******************************************/

