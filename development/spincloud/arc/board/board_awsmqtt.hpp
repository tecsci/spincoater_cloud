#ifndef BOARD_AWSMQTT_HPP_
#define BOARD_AWSMQTT_HPP_


/********************** inclusions *******************************************/

#include "Arduino.h"
#include "aws_iotcore_secrets.h"
#include "iot_config.hpp"
#include "hardware.hpp"

/********************** macros ***********************************************/


#define BOARD_AWSMQTT_MQTT_BUFF_BSIZE HARDWARE_AWSMQTT_MQTT_BUFF_BSIZE
/********************** typedef **********************************************/

typedef struct{

	char name[128];

}board_awsmqtt_pubtopic_t;

typedef struct{

	char name[128];
	void (*handler)(const char*);

}board_awsmqtt_subtopic_t;


typedef iotconfig_awsmqtt_pubtopic_tag_t board_awsmqtt_pubtopic_tag_t;
typedef iotconfig_awsmqtt_subtopic_tag_t board_awsmqtt_subtopic_tag_t;


/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_awsmqtt_config(void);


void board_awsmqtt_publish(board_awsmqtt_pubtopic_tag_t tag, const char * buff);
void board_awsmqtt_subscribe_set_handler(board_awsmqtt_subtopic_tag_t tag, void (*handler)(const char*));

bool board_awsmqtt_is_connected();

void board_awsmqtt_init(void);
void board_awsmqtt_deinit(void);
void board_awsmqtt_loop(void);

void board_awsmqtt_dummy_handler(const char* data);

#if 0
void board_mqtt_begin(const char * host_name, int port, WiFiClientSecure wifi_sec);
bool board_mqtt_connect(const char * cliente_id);
void board_mqtt_disconnect(void);
bool board_mqtt_is_connected();

void board_mqtt_publish(const char * topic, const char * msg);
void board_mqtt_subscribe(const char * topic);
bool board_mqtt_client_loop(void);
//WIP
void board_mqtt_re_connect(void);
void board_mqtt_stop(void);
bool board_mqtt_new_msg_check(char * msg);

#endif

#endif /* BOARD_AWSMQTT_HPP_ */
/********************** end of file ******************************************/
