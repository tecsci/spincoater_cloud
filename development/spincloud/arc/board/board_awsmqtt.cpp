/********************** inclusions *******************************************/

#include "board_awsmqtt.hpp"
//#include <ArduinoJson.h>
#include <MQTTClient.h>
#include <WiFiClientSecure.h>
#include "Arduino.h"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

MQTTClient client = MQTTClient(BOARD_AWSMQTT_MQTT_BUFF_BSIZE);
WiFiClientSecure net = WiFiClientSecure();

static struct {

	board_awsmqtt_subtopic_t sub_topics[BOARD_AWSMQTTSUB__N];
	board_awsmqtt_pubtopic_t pub_topics[BOARD_AWSMQTTPUB__N];

}self_;

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

void simple_msg_handler_(String &topic, String &payload) {

	for(int i=0;i<BOARD_AWSMQTTSUB__N;i++){
		if(0== strcasecmp(topic.c_str(),self_.sub_topics[i].name)){
			Serial.printf("encontrado el topic: fue el %d \n",i);
			//ACA LLAMAR A LOS HANDLERS
			self_.sub_topics[i].handler(payload.c_str());
		}
	}

  Serial.println("incoming: " + topic + " - " + payload);

	//  StaticJsonDocument<200> doc;
	//  deserializeJson(doc, payload);
	//  const char* message = doc["message"];
}

static void advanced_msg_handler_(char topic[], char bytes[], int length){
	return;
}


/********************** external functions definition ************************/

#if 0
void board_mqtt_begin(const char * host_name, int port, WiFiClientSecure wifi_sec){

	//client.begin(host_name, port, wifi_sec);

	client.begin(AWS_IOT_ENDPOINT, 8883, wifi_sec);

	// Create a message handler
	client.onMessage(simple_msg_handler_);

#if 0 //FOR ANDAVANCED HANDLER WIP TODO
	client.onMessageAdvanced(advanced_msg_handler_);
#endif
}

#if 0
void board_mqtt_begin(const char * host_name, int port, board_wifisecurity_config_t wifi_sec){

	client.begin(host_name, port, wifi_sec);

	// Create a message handler
	client.onMessage(simple_msg_handler_);

#if 0 //FOR ANDAVANCED HANDLER WIP TODO
	client.onMessageAdvanced(advanced_msg_handler_);
#endif
}
#endif

bool board_mqtt_connect(const char * cliente_id){

	return client.connect(THINGNAME);

}

bool board_mqtt_is_connected(){
	if(!client.connected()){
		return false;
	}

	return true;
}

void  board_mqtt_publish(const char * topic, const char * msg){
	client.publish(topic, msg);
}

void  board_mqtt_subscribe(const char * topic){
	client.subscribe(topic);
}


void board_mqtt_disconnect(void){

	client.disconnect();

}

void board_mqtt_re_connect(void) {

#if 0
	//BOARD_LOG("Attempting MQTT reconnection...");
     if (client.connect(self_.mqtt_client_id, BOARD_MQTT_USER, BOARD_MQTT_PASSWORD)) {
        client.subscribe(BOARD_MQTT_TOPIC_SUB);
        BOARD_LOG("MQTT client connected");

    } else {

    	BOARD_LOG("Disconnect MQTT Server");

    }
#endif
}

bool board_mqtt_client_loop(void) {

	client.loop();

}


void board_mqtt_stop(void) {

}

bool board_mqtt_new_msg_check(char * msg){

#if 0
	if (true == self_.read_flag)
	{
		strncpy(msg, self_.msg,BOARD_MQTT_MSG_SIZE);
		self_.read_flag = false;

		return true;

	}

	return false;
#endif
}

#endif

void board_awsmqtt_config(void){

	//WIFI SECURITY
	// Configure WiFiClientSecure to use the AWS IoT device credentials
	net.setCACert(BOARD_AWSMQTT_CERT_CA);
	net.setCertificate(BOARD_AWSMQTT_CERT_CRT);
	net.setPrivateKey(BOARD_AWSMQTT_CERT_PRIVATE);

	client.begin(BOARD_AWSMQTT_IOTCORE_ENDPOINT, 8883, net);

	// Create a message handler
	client.onMessage(simple_msg_handler_);

	Serial.print("Connecting to AWS IOT");

	/*
	while (!client.connect(BOARD_AWSMQTT_IOTCORE_THINGNAME)) {
		Serial.print(".");
		delay(100);
	}
	*/


	for(int i=0; i<60 ; i++ ){

		if(true == client.connect(BOARD_AWSMQTT_IOTCORE_THINGNAME)){
			break;
		}

		Serial.print(".");
		vTaskDelay(100);
	}


	if(!client.connected()){
		Serial.println("aws_iotcore timeout.");
		return;
	}
	else{
		Serial.println("aws_iotcore connected.");
	}

	// Subscribe to a topic
	//client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);

	for(int i = 0; i < BOARD_AWSMQTTSUB__N;i++){
		client.subscribe(self_.sub_topics[i].name);
	}

	Serial.println("aws_iotcore connected and subscribed");


}

void board_awsmqtt_publish(board_awsmqtt_pubtopic_tag_t tag, const char * buff)
{

	client.publish(self_.pub_topics[tag].name, buff);

  //client.publish(AWS_IOT_PUBLISH_TOPIC, buff);
}


void board_awsmqtt_subscribe_set_handler(board_awsmqtt_subtopic_tag_t tag, void (*handler)(const char*)){

	self_.sub_topics[tag].handler=handler;
}


bool board_awsmqtt_is_connected(){

	return client.connected();
}

void board_awsmqtt_init(void){


	//Set PUB Topics
	for(int i = 0; i < BOARD_AWSMQTTPUB__N; i++){
		strcpy(self_.pub_topics[i].name, IOTCONFIG_PUBTOPICS_VECTOR[i]);
	}

	//Set SUB Topics
	for(int j = 0; j < BOARD_AWSMQTTSUB__N; j++){
		strcpy(self_.sub_topics[j].name, IOTCONFIG_SUBTOPICS_VECTOR[j]);
		//init with dummy handler
		self_.sub_topics[BOARD_AWSMQTTSUB_LOG_TAG].handler=board_awsmqtt_dummy_handler;
	}

}

void board_awsmqtt_deinit(void){
	//board_mqtt_stop();
}

void board_awsmqtt_loop(void) {
	client.loop();
}

void board_awsmqtt_dummy_handler(const char* data){
	Serial.printf("dummy_handler: %s \n", data);
}


/********************** end of file ******************************************/
