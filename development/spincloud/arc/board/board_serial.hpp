#ifndef BOARD_SERIAL_HPP_
#define BOARD_SERIAL_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#include "hardware.hpp"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

enum
{
    BOARD_SERIAL_0, BOARD_SERIAL_1, BOARD_SERIAL_2, BOARD_SERIAL__N,
};

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

uint32_t board_serial_write(int serial_id, void *p_buffer, uint32_t size);

uint32_t board_serial_read(int serial_id, void *p_buffer, uint32_t size_max);

void board_serial_init(void);

void board_serial_deinit(void);

void board_serial_loop(void);

#endif /* BOARD_SERIAL_HPP_ */
/********************** end of file ******************************************/

