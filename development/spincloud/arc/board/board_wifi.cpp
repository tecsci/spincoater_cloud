/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "Arduino.h"
#include <WiFi.h>
#include "board_wifi.hpp"
//#include "board_log.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static struct
{
    bool enable;
} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_wifi_ip(uint8_t *p_ip)
{
    IPAddress wifi_ip = WiFi.localIP();
    for(int idx = 0; idx < 4; ++idx)
    {
        p_ip[idx] = wifi_ip[idx];
    }
}

void board_wifi_connect(const char *p_ssid, const char *p_password)
{
	  self_.enable = true;
	  WiFi.begin(p_ssid, p_password);
}

void board_wifi_disconnect(void)
{
    self_.enable = false;
    WiFi.disconnect(true);
}

bool board_wifi_is_connected(void)
{
    return WiFi.isConnected();
}

void board_wifi_init(void)
{
    //BOARD_LOG_TRACE_FUNCTION();
    WiFi.mode(WIFI_STA);
    self_.enable = false;
}

void board_wifi_deinit(void)
{
    //BOARD_LOG_TRACE_FUNCTION();
    WiFi.disconnect(true);
}

void board_wifi_reconnect(void)
{
    WiFi.reconnect();
}

/********************** end of file ******************************************/
