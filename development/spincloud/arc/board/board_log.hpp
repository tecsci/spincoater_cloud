#ifndef BOARD_LOG_HPP_
#define BOARD_LOG_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include "board_serial.hpp"

/********************** macros ***********************************************/

#define BOARD_LOG_ENABLE			(1)
#define BOARD_LOG_PRINT_BUFFER_LEN	(128)
#define BOARD_LOG_HEAD 				("/board/: ")
#define BOARD_LOG_NL 				("\n")
#define BOARD_LOG_SERIAL 			(BOARD_SERIAL_0)

#define BOARD_LOG_PRINT(...)\
	sprintf(board_log_buffer(), __VA_ARGS__);\
	board_log_print_buffer()

#define BOARD_LOG_PRINT_NL()			BOARD_LOG_PRINT(BOARD_LOG_NL)

#define BOARD_LOG_FORMAT(head, ...)\
	BOARD_LOG_PRINT(head);\
	BOARD_LOG_PRINT(__VA_ARGS__);\
	BOARD_LOG_PRINT_NL();

#if 1 == BOARD_LOG_ENABLE
#define BOARD_LOG(...) BOARD_LOG_FORMAT(BOARD_LOG_HEAD, __VA_ARGS__);
#else
#define BOARD_LOG(...)
#endif

#if 1 == BOARD_LOG_ENABLE
#define BOARD_LOG_TRACE_FUNCTION() BOARD_LOG("%s", __FUNCTION__);
#else
#define BOARD_LOG_TRACE_FUNCTION(...)
#endif

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_log_print(char* p_text);

void board_log_print_buffer(void);

char* board_log_buffer(void);

void board_log_enable(bool value);

void board_log_init(void);

void board_log_deinit(void);

void board_log_loop(void);

#endif /* BOARD_LOG_HPP_ */
/********************** end of file ******************************************/
