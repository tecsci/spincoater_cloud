#ifndef BOARD_TIMER_HPP_
#define BOARD_TIMER_HPP_

/********************** inclusions *******************************************/

#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

uint64_t board_timer_get_time_ms(void);
void board_timer_delay_ms_blocking(uint32_t ms);
void board_timer_delay_us_blocking(uint32_t us);

void board_timer_init(void);
void board_timer_deinit(void);
void board_timer_loop(void);

#endif /* BOARD_TIMER_HPP_ */
/********************** end of file ******************************************/
