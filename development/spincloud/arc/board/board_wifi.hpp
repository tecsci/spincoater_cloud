#ifndef BOARD_WIFI_HPP_
#define BOARD_WIFI_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_wifi_ip(uint8_t *p_ip);
void board_wifi_connect(const char *p_ssid, const char *p_password);
void board_wifi_disconnect(void);
bool board_wifi_is_connected(void);
void board_wifi_reconnect(void);

void board_wifi_init(void);
void board_wifi_deinit(void);

#endif /* BOARD_WIFI_HPP_ */
/********************** end of file ******************************************/

