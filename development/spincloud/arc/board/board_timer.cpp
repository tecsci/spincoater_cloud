/********************** inclusions *******************************************/

#include "board_timer.hpp"
#include "Arduino.h"
#include "board_log.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/


uint64_t board_timer_get_time_ms(void)
{
    return (uint64_t) (esp_timer_get_time() / 1000ULL);
}

void board_timer_delay_ms_blocking(uint32_t ms) {
	delay(ms); //--> es un vtaskdelay
	//delayMicroseconds(ms*1000);
}
void board_timer_delay_us_blocking(uint32_t us) {
	delayMicroseconds(us);
}

void board_timer_init(void)
{
    BOARD_LOG_TRACE_FUNCTION();
}

void board_timer_deinit(void)
{
}

void board_timer_loop(void)
{
}

/********************** end of file ******************************************/
