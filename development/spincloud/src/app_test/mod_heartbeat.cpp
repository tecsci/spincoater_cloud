 /********************** inclusions *******************************************/

#include "mod_heartbeat.hpp"

#include "../../../../device_config/os_config.hpp"
#include "logger.hpp"
#include "api.hpp"
#include "board.hpp"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/


void mod_heartbeat_init(void) {

	xTaskCreate(mod_heartbeat_task_loop,
			"mod heartbeat task", 				// A name just for humans
			OSCONFIG_MOD_HEARTBEAT_TASK_SIZE, // This stack size can be checked & adjusted by reading the Stack Highwater
			NULL,
			OSCONFIG_MOD_HEARTBEAT_TASK_PRIORITY,							 // Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			NULL);

}

void mod_heartbeat_deinit(void) {

}

void mod_heartbeat_task_loop(void *pvParameters) {

	for (;;) {

		vTaskDelay(OSCONFIG_MOD_HEARTBEAT_TASK_PERIOD);
	}
}



/********************** end of file ******************************************/
