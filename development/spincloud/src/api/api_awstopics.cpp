/********************** inclusions *******************************************/

#include "api_awstopics.hpp"
#include "logger.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

static struct{

	char aux_buff1[API_AWSTOPICS_BUFF_SZ];
	char aux_buff2[API_AWSTOPICS_BUFF_SZ];
	char aux_buff3[API_AWSTOPICS_BUFF_SZ];
	char aux_buff4[API_AWSTOPICS_BUFF_SZ];
	char aux_buff5[API_AWSTOPICS_BUFF_SZ];
	arclibs_json_doc aux_json1;
	arclibs_json_doc aux_json2;
	arclibs_json_doc aux_json3;
	arclibs_json_doc aux_json4;
	arclibs_json_doc aux_json5;

}self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void api_awstopics_send2timestream(api_awstopics_rawdata_t data){


	strcpy(self_.aux_buff1, "");

	self_.aux_json1["id"]=API_AWSTOPICS_DEVICE_ID;
	self_.aux_json1["loc"]=API_AWSTOPICS_LOCATION;

	self_.aux_json1["temp_rtd1"]=data.pt100_1_temperature;
	self_.aux_json1["temp_rtd2"]=data.pt100_2_temperature;

	self_.aux_json1["temp_bme"]=data.bme280_1_temperature;
	self_.aux_json1["hum_bme"]=data.bme280_1_humidity;
	self_.aux_json1["press_bme"]=data.bme280_1_pressure;

	self_.aux_json1["motor1_failed"]=data.motor_1_failed;
	self_.aux_json1["motor2_failed"]=data.motor_2_failed;
	self_.aux_json1["door_is_open"]=data.door_is_open;

	self_.aux_json1["stime"]=data.sample_time;
	self_.aux_json1["fault"]=data.fault;

	self_.aux_json1["rtd1_id"]=data.pt100_1_id;
	self_.aux_json1["rtd2_id"]= data.pt100_2_id;
	self_.aux_json1["bme_id"]=data.bme280_id;

	if(false == arclibs_json_serialize(self_.aux_buff1,self_.aux_json1, API_AWSTOPICS_BUFF_SZ)){
		ERROR_LOG("api_awstopics_send2timestream JSON SERIALIZE ERROR");
	}
	//strcpy(aux_buff, "{\"id\":\"dev0\",\"loc\":\"x\",\"temp\":22,\"hum\":85,\"stime\":85,\"log\":\"testing\"}");

	Serial.print(self_.aux_buff1);


	if(true == data.calib_mode){
		DEBUG_LOG(" ok atoi is_calib_mode: true: |%d|", data.calib_mode);
	}
	else{
		DEBUG_LOG(" ok atoi is_calib_mode: false: |%d|", data.calib_mode);
	}


	//board_awsmqtt_publish(BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM_TAG,self_.aux_buff1);
	DEBUG_LOG("send msg to");
	if(false == data.calib_mode){
		board_awsmqtt_publish(BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM_TAG,self_.aux_buff1);
		DEBUG_LOG("BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM");
	}
	else{
		board_awsmqtt_publish(BOARD_AWSMQTTPUB_CALIBRATIONMODE_TAG,self_.aux_buff1);
		DEBUG_LOG("BOARD_AWSMQTTPUB_CALIBRATIONMODE");
	}

}
void api_awstopics_send2stimestream_alarm(api_awstopics_alarmdata_t data){

	strcpy(self_.aux_buff3, "");

	self_.aux_json3["motor1_failed"]=data.motor_1_failed;
	self_.aux_json3["motor2_failed"]=data.motor_2_failed;
	self_.aux_json3["door_is_open"]=data.door_is_open;

	self_.aux_json3["stime"]=data.sample_time;
	self_.aux_json3["fault"]=data.fault;


	arclibs_json_serialize(self_.aux_buff3,self_.aux_json3, API_AWSTOPICS_BUFF_SZ);

	board_awsmqtt_publish(BOARD_AWSMQTTPUB_ALARMS2TIMESTREAM_TAG,self_.aux_buff3);


}


void api_awstopics_send2sns(api_awstopics_alarmdata_t data){

	strcpy(self_.aux_buff4, "");

	self_.aux_json4["motor1_failed"]=data.motor_1_failed;
	self_.aux_json4["motor2_failed"]=data.motor_2_failed;
	self_.aux_json4["door_is_open"]=data.door_is_open;

	self_.aux_json4["stime"]=data.sample_time;
	self_.aux_json4["fault"]=data.fault;


	arclibs_json_serialize(self_.aux_buff4,self_.aux_json4, API_AWSTOPICS_BUFF_SZ);

	board_awsmqtt_publish(BOARD_AWSMQTTPUB_ALARMS2SNS_TAG,self_.aux_buff4);


}

void api_awstopics_send_handshake(){

	strcpy(self_.aux_buff5, "");

	self_.aux_json5["id"]=API_AWSTOPICS_DEVICE_ID;
	self_.aux_json5["loc"]=API_AWSTOPICS_LOCATION;
	self_.aux_json5["msg"]="handshake";


	arclibs_json_serialize(self_.aux_buff5,self_.aux_json5, API_AWSTOPICS_BUFF_SZ);

	board_awsmqtt_publish(BOARD_AWSMQTTPUB_HANDSHAKE_TAG,self_.aux_buff5);

}


void api_awstopics_init(void)
{

}


/********************** end of file ******************************************/

