#ifndef API_SERIAL_HPP_
#define API_SERIAL_HPP_

/********************** inclusions *******************************************/

#include <stdint.h>
#include "../arc/board.hpp"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

enum
{
    API_SERIAL_UART0,
    API_SERIAL_UART1,
    API_SERIAL_UART2,
    API_SERIAL_CONSOLE = API_SERIAL_UART0,
};

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

uint32_t api_serial_write(int serial_id, void* p_buffer, uint32_t size);

uint32_t api_serial_read(int serial_id, void* p_buffer, uint32_t size_max);

uint32_t api_serial_swrite(int serial_id, char* p_text);

uint32_t api_serial_sread(int serial_id, char* p_text, uint32_t size_max);

uint32_t api_serial_console_cwrite(char chr);

uint32_t api_serial_console_swrite(char* p_text);

uint32_t api_serial_console_write(void* p_buffer, uint32_t size);

uint32_t api_serial_console_sread(char* p_text, uint32_t size_max);

void api_serial_init(void);

void api_serial_deinit(void);

void api_serial_loop(void);

#endif /* API_SERIAL_HPP_ */
/********************** end of file ******************************************/

