/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "../arc/board.hpp"
#include "api_log.hpp"
#include "api_serial.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

uint32_t api_serial_write(int serial_id, void* p_buffer, uint32_t size)
{
    return board_serial_write(serial_id, p_buffer, size);
}

uint32_t api_serial_read(int serial_id, void* p_buffer, uint32_t size_max)
{
    return board_serial_read(serial_id, p_buffer, size_max);
}

uint32_t api_serial_swrite(int serial_id, char* p_text)
{
    return api_serial_write(serial_id, p_text, strlen(p_text));
}

uint32_t api_serial_sread(int serial_id, char* p_text, uint32_t size_max)
{
    if(0 == size_max)
    {
        return 0;
    }
    uint32_t size = api_serial_read(serial_id, p_text, size_max - 1);
    p_text[size] = '\0';
    return size;
}

uint32_t api_serial_console_cwrite(char chr)
{
    return api_serial_write(API_SERIAL_UART0, (void*)&chr, 1);
}

uint32_t api_serial_console_swrite(char* p_text)
{
    return api_serial_swrite(API_SERIAL_UART0, p_text);
}

uint32_t api_serial_console_write(void* p_buffer, uint32_t size)
{
    return api_serial_write(API_SERIAL_UART0, p_buffer, size);
}

uint32_t api_serial_console_sread(char* p_text, uint32_t size_max)
{
    return api_serial_sread(API_SERIAL_UART0, p_text, size_max);
}

void api_serial_init()
{
    API_LOG_TRACE_FUNCTION();
}

void api_serial_deinit()
{
    API_LOG_TRACE_FUNCTION();
}

void api_serial_loop()
{

}

/********************** end of file ******************************************/

