#ifndef API_AWSTOPICS_HPP_
#define API_AWSTOPICS_HPP_

/********************** inclusions *******************************************/

#include "board.hpp"
#include "json_lib.hpp"
#include "device_info.hpp"
#include <stdlib.h>
#include <stdint.h>

/********************** macros ***********************************************/

#define  API_AWSTOPICS_DEVICE_ID DEVICEINFO_CLIENT_NAME "_" DEVICEINFO_ID_LETTER DEVICEINFO_ID_NUM
#define  API_AWSTOPICS_LOCATION DEVICEINFO_LOCATION

#define API_AWSTOPICS_BUFF_SZ 512
#define  API_AWSTOPICS_LOG_MAX_SZ 64

/********************** typedef **********************************************/

typedef struct{

	time_t sample_time;

	/* PT100 1*/
	float pt100_1_temperature;
	uint64_t pt100_1_id;

	/* PT100 2*/
	float pt100_2_temperature;
	uint64_t pt100_2_id;

	/* BME208_1*/
	float bme280_1_temperature;
	float bme280_1_humidity;
	float bme280_1_pressure;
	uint64_t bme280_id;

	/* MOTORS*/
	bool motor_1_failed;
	bool motor_2_failed;
	bool door_is_open;

	/* FAULT */
	bool fault;

	/*CALIBRATION MODE*/
	bool calib_mode;

}api_awstopics_rawdata_t;

typedef struct{

	time_t sample_time;

	/* MOTORS*/
	bool motor_1_failed;
	bool motor_2_failed;
	bool door_is_open;

	/* FAULT */
	bool fault;

}api_awstopics_alarmdata_t;

/*TIMESTEREAM payload:
	{"id":"dev0",
	"loc":"x",
	"temp_rtd1": 22,
	"temp_rtd2": 22,
	"temp_rtd3": 22,
	"temp_bme": 22,
	"hum_bme":80,
	"press_bme":80,
	"motor1_failed":1,
	"motor2_failed":1,
	"door_is_open":1,
	"stime":1000,
	"fault": 0
 }
 */


/*FIREBASE payload:
 *{
	"id": "aws_test",
	"stime": "10",
	"temp1": "22",
	"hum1": "23",
	"temp2": "24",
	"hum2": "25"
	"log":"nan"
}
 */

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void api_awstopics_send2timestream(api_awstopics_rawdata_t data);
void api_awstopics_send2stimestream_alarm(api_awstopics_alarmdata_t data);
void api_awstopics_send2sns(api_awstopics_alarmdata_t data);
void api_awstopics_send_handshake();
void api_awstopics_init(void);

#endif /* API_AWSTOPICS_HPP_ */
/********************** end of file ******************************************/

