/********************** inclusions *******************************************/

#include <stdlib.h>
#include "api_sdlog.hpp"
#include "api_sd.hpp"
#include "api_file.hpp"
#include "api_localtime.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

char g_api_sdlog_print_buffer[];

/********************** internal functions definition ************************/

static struct{

}self_;

/********************** external functions definition ************************/

void api_sdlog_swrite(char *p_str)
{
    //api_serial_swrite(API_SERIAL_CONSOLE, p_str);
	api_file_swrite(API_SDLOG_FILE_TAG, p_str);
}

long api_sdlog_get_time(void)
{
	time_t aux_time;
	if(false == api_localtime_get_actual_utc_timestamp(&aux_time)){
		aux_time = 0;
	}

    return aux_time;
}

void api_sdlog_file_open(void){
	api_file_open(API_SDLOG_FILE_TAG, API_SDLOG_FILE_PATH, API_FILE_OPENMODE_APPEND);
}

void api_sdlog_init(void){
	api_sdlog_file_open();
}
void api_sdlog_deinit(void){

	api_file_close(API_SDLOG_FILE_TAG);


}


/********************** end of file ******************************************/
