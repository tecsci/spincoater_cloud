/********************** inclusions *******************************************/

#include <stdlib.h>
#include "api_log.hpp"
#include "api_serial.hpp"
#include "api_timer.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

char g_api_log_print_buffer[];

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void api_log_swrite(char *p_str)
{
    api_serial_swrite(API_SERIAL_CONSOLE, p_str);
}

unsigned int api_log_get_time_ms(void)
{
    return (int)api_timer_get_walltime_ms();
}

void api_log_init(void)
{
    return;
}

void api_log_deinit(void)
{
    return;
}

void api_log_loop(void)
{

}

/********************** end of file ******************************************/
