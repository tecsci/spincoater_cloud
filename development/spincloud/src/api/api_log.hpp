#ifndef API_LOG_HPP_
#define API_LOG_HPP_

/********************** inclusions *******************************************/

#include <stdlib.h>
#include <stdio.h>
#include "../lib/logger.hpp"

/********************** macros ***********************************************/

#define API_LOG_ENABLE                      (1)
#define API_LOG_TRACE_ENABLE                (1)
#define API_LOG_TIMESTAMP_ENABLE            (1)
#define API_LOG_PRINT_BUFFER_LEN            (128)
#define API_LOG_HEAD                        ("/api/: ")
#define API_LOG_NL                          ("\n")

#define API_LOG_PRINT(...)\
    snprintf(g_api_log_print_buffer, API_LOG_PRINT_BUFFER_LEN,  __VA_ARGS__);\
    api_log_swrite(g_api_log_print_buffer)

#define API_LOG_PRINT_NL()                  API_LOG_PRINT(API_LOG_NL)

#if API_LOG_ENABLE && API_LOG_TIMESTAMP_ENABLE
#define API_LOG_PRINT_TIMESTAMP()           API_LOG_PRINT("[%06u] ", api_log_get_time_ms())
#else
#define API_LOG_PRINT_TIMESTAMP()
#endif

#define API_LOG_FORMAT(head, ...)\
    API_LOG_PRINT_TIMESTAMP();\
	API_LOG_PRINT(head);\
	API_LOG_PRINT(__VA_ARGS__);\
	API_LOG_PRINT_NL();

#if API_LOG_ENABLE
#define API_LOG(...)                        API_LOG_FORMAT(API_LOG_HEAD, __VA_ARGS__);
#else
#define API_LOG(...)
#endif

#if API_LOG_ENABLE && API_LOG_TRACE_ENABLE
#define API_LOG_TRACE_FUNCTION()            API_LOG("%s", __FUNCTION__);
#else
#define API_LOG_TRACE_FUNCTION(...)
#endif

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

extern char g_api_log_print_buffer[API_LOG_PRINT_BUFFER_LEN];

/********************** external functions declaration ***********************/

void api_log_swrite(char*);
unsigned int api_log_get_time_ms(void);

void api_log_init(void);
void api_log_deinit(void);
void api_log_loop(void);

#endif /* API_LOG_HPP_ */
/********************** end of file ******************************************/
