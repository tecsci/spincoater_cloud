#ifndef API_SDLOG_HPP_
#define API_SDLOG_HPP_

/********************** inclusions *******************************************/

#include <stdlib.h>
#include <stdio.h>

#include "os_config.hpp"
#include "func_config.hpp"
#include "../lib/logger.hpp"

/********************** macros ***********************************************/

#define API_SDLOG_FILE_PATH FUNCCONFIG_FILEPATH_DEVICE_LOG
#define API_SDLOG_FILE_TAG API_FILE_SD_LOGGER

#define API_SDLOG_ENABLE                      (1)
#define API_SDLOG_TRACE_ENABLE                (1)
#define API_SDLOG_NTP_TIMESTAMP_ENABLE        (1)
#define API_SDLOG_PRINT_BUFFER_LEN            (128)
#define API_SDLOG_HEAD                        ("/api/: ")
#define API_SDLOG_NL                          ("\n")

#define API_SDLOG_PRINT(...)\
    snprintf(g_api_sdlog_print_buffer, API_SDLOG_PRINT_BUFFER_LEN,  __VA_ARGS__);\
    api_sdlog_swrite(g_api_sdlog_print_buffer)

#define API_SDLOG_PRINT_NL()	API_SDLOG_PRINT(API_SDLOG_NL)

#if API_SDLOG_ENABLE && API_SDLOG_NTP_TIMESTAMP_ENABLE
#define API_SDLOG_PRINT_NTP_TIMESTAMP()           API_SDLOG_PRINT("[%06u] ", api_sdlog_get_time())
#else
#define API_SDLOG_PRINT_NTP_TIMESTAMP()
#endif

#define API_SDLOG_FORMAT(head, ...)\
    API_SDLOG_PRINT_NTP_TIMESTAMP();\
	API_SDLOG_PRINT(head);\
	API_SDLOG_PRINT(__VA_ARGS__);\
	API_SDLOG_PRINT_NL();

#if API_SDLOG_ENABLE
#define API_SDLOG(...)                        API_SDLOG_FORMAT(API_SDLOG_HEAD, __VA_ARGS__);
#else
#define API_SDLOG(...)
#endif

#if API_SDLOG_ENABLE && API_SDLOG_TRACE_ENABLE
#define API_SDLOG_TRACE_FUNCTION()            API_SDLOG("%s", __FUNCTION__);
#else
#define API_SDLOG_TRACE_FUNCTION(...)
#endif

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

extern char g_api_sdlog_print_buffer[API_SDLOG_PRINT_BUFFER_LEN];

/********************** external functions declaration ***********************/

void api_sdlog_swrite(char*);
long api_sdlog_get_time(void);

void api_sdlog_file_open(void);
void api_sdlog_init(void);
void api_sdlog_deinit(void);

#endif /* API_SDLOG_HPP_ */
/********************** end of file ******************************************/
