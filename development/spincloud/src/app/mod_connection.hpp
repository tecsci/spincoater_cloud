#ifndef _APP_MOD_CONNECTION_HPP_
#define _APP_MOD_CONNECTION_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "../api/api.hpp"

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/


void mod_connection_set_ssid(const char * ssid);
void mod_connection_set_pass(const char * pass);

bool mod_connection_wifi_is_connected(void);
bool mod_connection_aws_is_connected(void);

void mod_connection_init(void);
void mod_connection_deinit(void);
void mod_connection_task_loop(void *pvParameters);


void mod_connection_connection_enable(bool enable);
bool mod_connection_is_enable();

#if 0 //FUTURE VERSION
void mod_connection_tcp_comm_enable(bool enable);
#endif

#endif /* _APP_MOD_CONNECTION_HPP_ */
/********************** end of file ******************************************/

