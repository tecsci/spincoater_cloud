/********************** inclusions *******************************************/
#include "mod_connection.hpp"

#include "os_config.hpp"
#include "func_config.hpp"
#include "logger.hpp"
#include "api.hpp"
#include "board.hpp"

/********************** macros and definitions *******************************/

#define MOD_CONNECTION_WIFI_CONNECT_RETRIES_N 1000
#define MOD_CONNECTION_AWSMQTT_CONNECT_LOOP 10
#define MOD_CONNECTION_WIFI_BUFFERS_SZ 50

#define MOD_CONNECTION_WIFI_SSID 				FUNCCONFIG_DEFAULT_WIFIST_SSID
#define MOD_CONNECTION_WIFI_PASSWORD 			FUNCCONFIG_DEFAULT_WIFIST_PASS

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/
static struct
{
	char aux_str[50];
    bool enable;
#if 0 //FUTURE VERSION
    bool tcp_enable;
    bool tcp_first_connect;
#endif

    char ssid[MOD_CONNECTION_WIFI_BUFFERS_SZ]=MOD_CONNECTION_WIFI_SSID;
    char pass[MOD_CONNECTION_WIFI_BUFFERS_SZ]=MOD_CONNECTION_WIFI_PASSWORD;

    int wifi_connection_retries;
    int aws_connection_retries;

} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

void mod_connection_aws_log_handler(const char* data){
	Serial.printf("log handler: %s \n", data);
}

void mod_connection_connect()
{
	DEBUG_LOG("mod_connection_connect -->: ssid:%s|pass:%s|",self_.ssid,self_.pass);
	DEBUG_LOG("mod_connection_connect -->: ssid:%d|pass:%d|",strlen(self_.ssid),strlen(self_.pass));
	board_wifi_connect(self_.ssid,self_.pass);
}

/********************** external functions definition ************************/




void mod_connection_set_ssid(const char * p_ssid){
	strcpy(self_.ssid,p_ssid);
}

void mod_connection_set_pass(const char * p_pass){
	strcpy(self_.pass,p_pass);
}

void mod_connection_connection_enable(bool enable)
{
	self_.enable = enable;
}


bool mod_connection_is_enable(){
	return self_.enable;
}


#if 0 //FUTURE VERSION
void mod_connection_tcp_comm_enable(bool enable){

	self_.tcp_enable=enable;

}
#endif


bool mod_connection_wifi_is_connected(void)
{
	return board_wifi_is_connected();
}

bool mod_connection_aws_is_connected(void)
{
	return board_awsmqtt_is_connected();
}



void mod_connection_init(void) {

	xTaskCreate(mod_connection_task_loop,
			"mod connection task", 			    		// A name just for humans
			OSCONFIG_MOD_CONNECTION_TASK_SIZE,  		// This stack size can be checked & adjusted by reading the Stack Highwater
			NULL,
			OSCONFIG_MOD_CONNECTION_TASK_PRIORITY, 	// Priority, with 3 (configMAX_PRIORITIES - 1) being the highest, and 0 being the lowest.
			NULL);
}

void mod_connection_deinit(void) {

}

void mod_connection_task_loop(void *pvParameters) {

	mod_connection_connection_enable(false);

	self_.wifi_connection_retries=0;

	strcpy(self_.ssid,MOD_CONNECTION_WIFI_SSID);
	strcpy(self_.pass,MOD_CONNECTION_WIFI_PASSWORD);

	bool aux_led_state=false;
	board_gpio_write(BOARD_GPIOOUT_LED1, aux_led_state);

	for (;;) {

		//TRACE_LOG_FUCTION();

		if (true == self_.enable)
		{

			if(false == mod_connection_wifi_is_connected()){

				self_.wifi_connection_retries++;

				mod_connection_connect();
				DEBUG_LOG("wifi not connected");
				while (false == board_wifi_is_connected()){

					board_gpio_write(BOARD_GPIOOUT_LED1, true);

					self_.wifi_connection_retries++;

					vTaskDelay(500);
					DEBUG_LOG("wifi not connected");

					if(MOD_CONNECTION_WIFI_CONNECT_RETRIES_N == self_.wifi_connection_retries){

						MONITOR_LOG("wifi connection retries = %d", MOD_CONNECTION_WIFI_CONNECT_RETRIES_N);
						MONITOR_LOG("restarting in 10 sesc");

						vTaskDelay(10*1000);
						board_reset_restart();
					}

				}
				DEBUG_LOG("wifi connected");
				self_.wifi_connection_retries=0;


				//NTP UPDATE LOCAL TIME
				api_localtime_update_loop();

				if(true == api_localtime_first_update_done()){
					//LOG INTO SD
				}

			}
			//WIFI IS CONNECTED
			else{

				//AWSMQTT CONNECT
				if((false == board_awsmqtt_is_connected())){
					board_gpio_write(BOARD_GPIOOUT_LED1, true);
					DEBUG_LOG("aws not connected");
					board_awsmqtt_config();
					//board_awsmqtt_subscribe_set_handler(BOARD_AWSMQTTSUB_LOG_TAG, mod_connection_aws_log_handler);
				}
				else{
					aux_led_state=!aux_led_state;
					board_gpio_write(BOARD_GPIOOUT_LED1, aux_led_state);
					board_awsmqtt_loop();
					api_localtime_update_loop();
				}
			}
		}

		vTaskDelay(OSCONFIG_MOD_CONNECTION_TASK_PERIOD);
	}
}
/********************** end of file ******************************************/
/********************** end of file ******************************************/
