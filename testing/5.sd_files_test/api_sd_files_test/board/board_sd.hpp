/*
* Copyright (c) 2020 Lucas Mancini <mancinilucas95@gmail.com>.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from
*    this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* @file	board_sd.hpp
* @date	2020-26-10
* @author:
*  - Lucas Mancini <mancinilucas95@gmail.com>
* @version	v1.0.0
*/

#ifndef ARC_BOARD_BOARD_SD_HPP_
#define ARC_BOARD_BOARD_SD_HPP_

/********************** inclusions *******************************************/

#include "Arduino.h"
#include "SD.h"
#include "FS.h"
#include "SPI.h"

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

/********************** macros ***********************************************/

#define BOARD_SD_NUMBER_OF_FILES    (2)

/********************** typedef **********************************************/

typedef enum{
	BOARD_SD_WRITE_MODE,
	BOARD_SD_APPEND_MODE,
	BOARD_SD_READ_MODE
}board_sd_mode_t;

#ifdef WB_ARC_ARDUINO
typedef sdcard_type_t board_sdcard_type_t;
typedef File board_file_t;
#else
typedef int board_sdcard_type_t;
typedef int board_file_t;

#endif

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

board_file_t board_sd_file_open(const char * path, const char *  mode ); //(mode = w, a, r)
bool board_sd_file_exists(const char * path );
void board_sd_file_delete(const char * path );
uint32_t board_sd_file_sread_line(board_file_t file , char * buffer, uint32_t line_n);
bool board_sd_file_seek( board_file_t file , uint32_t position );
int board_sd_file_available( board_file_t file);
bool board_sd_is_mounted(void);

bool board_sd_file_open_read_mode(int file_index, const char* p_path);
bool board_sd_file_open_write_mode(int file_index, const char* p_path);
bool board_sd_file_open_append_mode(int file_index, const char* p_path);

void board_sd_file_close(int file_index);
uint32_t board_sd_file_swrite(int file_index, const char* p_text);
uint32_t board_sd_file_sread(int file_index, char* p_buffer, uint32_t size);

void board_sd_list_root_files();
void board_sd_init();


#endif /* ARC_BOARD_BOARD_SD_HPP_ */
/********************** end of file ******************************************/

