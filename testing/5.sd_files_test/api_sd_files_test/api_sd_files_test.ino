#include "Arduino.h"
#include "board/board_sd.hpp"
#include "board/board_serial.hpp"
#include "board/board_log.hpp"
#include "api/api_file.hpp"

char aux_buff[512];

void setup()
{

	board_serial_init();
	board_log_init();

	board_sd_init();


	if(true == board_sd_is_mounted()){
		Serial.println("board mounted!");
	}
	else{
		Serial.println("board not mounted!");
		while(1);
	}


	board_sd_list_root_files();

	api_file_init();

	Serial.println("OPENING IN READ MODE");
	if(false == api_file_open(API_FILE_WIFI_CREDENTIALS, "/wifi_credentials.txt", API_FILE_OPENMODE_READ)){
		Serial.println("error opening file");
		while(1);
	}


	Serial.println("READING 1 ");
	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, aux_buff, 512)){
		Serial.println(aux_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(aux_buff);
	}

	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, aux_buff, 512)){
		Serial.println(aux_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(aux_buff);
	}

	Serial.println("CLOSE AND OPEN IN APPEND MODE");

	api_file_close(API_FILE_WIFI_CREDENTIALS);

	if(false == api_file_open(API_FILE_WIFI_CREDENTIALS, "/wifi_credentials.txt", API_FILE_OPENMODE_APPEND)){
		Serial.println("error opening file");
		while(1);
	}

	Serial.println("WRITING");

	api_file_swrite(API_FILE_WIFI_CREDENTIALS, "---> WRITING here");
	api_file_swrite(API_FILE_WIFI_CREDENTIALS, "--> WRITING here 2");


	Serial.println("CLOSE AND OPEN IN READ MODE");

	api_file_close(API_FILE_WIFI_CREDENTIALS);

	if(false == api_file_open(API_FILE_WIFI_CREDENTIALS, "/wifi_credentials.txt", API_FILE_OPENMODE_READ)){
		Serial.println("error opening file");
		while(1);
	}

	Serial.println("READING 2 ");
	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, aux_buff, 512)){
		Serial.println(aux_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(aux_buff);
	}

	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, aux_buff, 512)){
		Serial.println(aux_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(aux_buff);
	}

	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, aux_buff, 512)){
		Serial.println(aux_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(aux_buff);
	}

	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, aux_buff, 512)){
		Serial.println(aux_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(aux_buff);
	}

	Serial.println("CLOSE");

	api_file_close(API_FILE_WIFI_CREDENTIALS);


}

// The loop function is called in an endless loop
void loop()
{
//Add your repeated code here
}
