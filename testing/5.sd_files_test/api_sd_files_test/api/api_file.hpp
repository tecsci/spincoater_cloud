/*
 * Copyright (c) 2021 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file   : api_file.hpp
 * @date   : Feb 2, 2021
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

#ifndef BLENODE_API_API_FILE_HPP_
#define BLENODE_API_API_FILE_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/********************** macros ***********************************************/

#define API_FILE_NEWLINE_CHAR            ('\n')

typedef enum
{
    API_FILE_WIFI_CREDENTIALS,
    API_FILE_IOT_PARAMETERS,
    API_FILE__N,
} api_file_t;

typedef enum
{
    API_FILE_OPENMODE_READ,
    API_FILE_OPENMODE_WRITE,
    API_FILE_OPENMODE_APPEND,
    API_FILE_OPENMODE__N,
} api_file_open_mode_t;

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

bool api_file_open(api_file_t file, const char* p_path, api_file_open_mode_t mode);
void api_file_close(api_file_t file);

uint32_t api_file_swrite(api_file_t file, const char* p_text);
uint32_t api_file_swrite_line(api_file_t file, const char* p_text);
uint32_t api_file_sread(api_file_t file, char* p_text, uint32_t size_max);

bool api_file_read_char(api_file_t file, char* p_chr);
uint32_t api_file_sread_line(api_file_t file, char* p_text, uint32_t size_max);

void api_file_init(void);


#endif /* BLENODE_API_API_FILE_HPP_ */
/********************** end of file ******************************************/

