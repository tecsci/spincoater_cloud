/*
 * Copyright (c) 2021 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : api_file.cpp
 * @date   : Feb 2, 2021
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "../board/board_sd.hpp"
//#include "api_log.hpp"
#include "api_file.hpp"

/********************** macros and definitions *******************************/

#if 0
#define ERRRO_INVALID_FILE_()\
    if(API_FILE__N <= file)\
    {\
        API_LOG_TRACE_FUNCTION();\
        API_LOG("ERROR: Invalid file");\
        while(1);\
    }
#endif

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static struct{

	bool init_ok;

}self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

uint32_t write_newline_(api_file_t file)
{
    //ERRRO_INVALID_FILE_();
    char newline[2] = {API_FILE_NEWLINE_CHAR, '\0'};
    return api_file_swrite(file, newline);
}

/********************** external functions definition ************************/

bool api_file_open(api_file_t file, const char* p_path, api_file_open_mode_t mode)
{
    //ERRRO_INVALID_FILE_();
    switch (mode)
    {
        case API_FILE_OPENMODE_READ:
            return board_sd_file_open_read_mode(file, p_path);
            break;
        case API_FILE_OPENMODE_WRITE:
            return board_sd_file_open_write_mode(file, p_path);
            break;
        case API_FILE_OPENMODE_APPEND:
            return board_sd_file_open_append_mode(file, p_path);
            break;
        default:
            return false;
            break;
    }
}

void api_file_close(api_file_t file)
{
    //ERRRO_INVALID_FILE_();
    return board_sd_file_close(file);
}

uint32_t api_file_swrite(api_file_t file, const char* p_text)
{
    //ERRRO_INVALID_FILE_();
    return board_sd_file_swrite(file, p_text);
}

uint32_t api_file_swrite_line(api_file_t file, const char* p_text)
{
    //ERRRO_INVALID_FILE_();
    uint32_t ret;
    ret = api_file_swrite(file, p_text);
    ret += write_newline_(file);
    return ret;
}

uint32_t api_file_sread(api_file_t file, char* p_text, uint32_t size)
{
    //ERRRO_INVALID_FILE_();
    return board_sd_file_sread(file, p_text, size);
}

bool api_file_read_char(api_file_t file, char* p_chr)
{
    //ERRRO_INVALID_FILE_();
    if(0 == api_file_sread(file, p_chr, 1))
    {
        return false;
    }
    return true;
}

uint32_t api_file_sread_line(api_file_t file, char* p_text, uint32_t size_max)
{
    //ERRRO_INVALID_FILE_();
    uint32_t idx = 0;
    char chr;
    while((true == api_file_read_char(file, &chr))
            && (API_FILE_NEWLINE_CHAR != chr)
            && (idx < size_max))
    {
        p_text[idx] = chr;
        idx++;
    }
    if(idx < size_max)
    {
        p_text[idx] = '\0';
    }
    return idx;
}

bool api_file_is_init_ok(){
	return self_.init_ok;
}

void api_file_init(void)
{

	self_.init_ok=true;

    //API_LOG_TRACE_FUNCTION();
    if(BOARD_SD_NUMBER_OF_FILES < API_FILE__N)
    {
        //API_LOG("ERROR: BOARD_SD_MEMORY_FILE_MAX < API_FILE__N");
    	//while(1);
    	Serial.println("ERROR: BOARD_SD_MEMORY_FILE_MAX < API_FILE__N");

    	self_.init_ok=false;
    }
    else if(BOARD_SD_NUMBER_OF_FILES != API_FILE__N)
    {
    	Serial.println("WARNING: BOARD_SD_MEMORY_FILE_MAX != API_FILE__N");
    }

    if(false == board_sd_is_mounted()){
    	self_.init_ok=false;
    }


}

/********************** end of file ******************************************/
