/*
 * Copyright (c) 2020 Lucas Mancini <mancinilucas95@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * @file	board_sd.cpp
 * @date	2020-16-10
 * @author:
 *  - Lucas Mancini <mancinilucas95@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include "board_sd.hpp"
#include "board_log.hpp"
#include  "board_spi.hpp"
#include "Arduino.h"
#include "SD.h"
#include "FS.h"
#include "SPI.h"

/********************** macros and definitions *******************************/


#define BOARD_SD_CS_PIN 18

#define BOARD_SD_OPEN_READ_MODE    ("r")
#define BOARD_SD_OPEN_WRITE_MODE   ("w")
#define BOARD_SD_OPEN_APPEND_MODE  ("a")

/********************** internal data declaration ****************************/

#if 0 //Not in use
static board_sdcard_type_t board_sd_cardtype;
static uint8_t board_sd_cardsize;
#endif

/********************** internal data definition *****************************/

static struct
{

    bool is_sd_mounted;

    File files[BOARD_SD_NUMBER_OF_FILES];

} self_;


board_spi_t board_sd_self_spi;
/********************** external data definition *****************************/

/********************** internal functions definition ************************/





static void board_sd_setup_(void)
{

	board_sd_self_spi = board_spi_get_spi_pointer();

	if(NULL == board_sd_self_spi){
		BOARD_LOG("NULL POINTER");
		return;
	}


    if (!SD.begin(BOARD_SD_CS_PIN,*board_sd_self_spi))
    {
        //error mount
        BOARD_LOG("Card Mount Failed");
        self_.is_sd_mounted = false;
        return;
    }
    else
    {
        BOARD_LOG("Card Mounted successfully");
        self_.is_sd_mounted = true;
    }

}

static void board_list_files_opcs_(fs::FS &fs, const char * dirname, uint8_t levels){
  Serial.printf("Listing directory: %s\n", dirname);

  File root = fs.open(dirname);
  if(!root){
    Serial.println("Failed to open directory");
    return;
  }
  if(!root.isDirectory()){
    Serial.println("Not a directory");
    return;
  }

  File file = root.openNextFile();
  while(file){
    if(file.isDirectory()){
      Serial.print("  DIR : ");
      Serial.println(file.name());
      if(levels){
    	  board_list_files_opcs_(fs, file.name(), levels -1);
      }
    } else {
      Serial.print("  FILE: ");
      Serial.print(file.name());
      Serial.print("  SIZE: ");
      Serial.println(file.size());
    }
    file = root.openNextFile();
  }
}


/********************** external functions definition ************************/

/******************** WIP **********************************/

bool board_sd_file_exists(const char *path)
{
    return false;
}

void board_sd_file_delete(const char *path)
{
	return;
}

bool board_sd_file_seek(board_file_t board_file, uint32_t position)
{
    return false;
}

int board_sd_file_available(board_file_t board_file)
{
    return 0;
}


/*********************************************************/

bool board_sd_is_mounted(void)
{
    return self_.is_sd_mounted;
}


void board_sd_list_root_files(){
	board_list_files_opcs_(SD, "/", 0);
}

bool board_sd_file_open_read_mode(int file_index, const char *p_path)
{

    if ((BOARD_SD_NUMBER_OF_FILES - 1) < file_index)
    {
        BOARD_LOG("Open r failed - Invalid file index");
        return false;
    }

    if (NULL == p_path)
    {
        BOARD_LOG("Open r failed - NULL Pointer");
        return false;
    }

    self_.files[file_index] = SD.open(p_path, BOARD_SD_OPEN_READ_MODE);

    if (!(self_.files[file_index]))
    {
        BOARD_LOG("Open r failed - Invalid file");
        return false;
    }
    else
    {
        BOARD_LOG("Open r successfully");
        return true;
    }

}

bool board_sd_file_open_write_mode(int file_index, const char *p_path)
{

    if ((BOARD_SD_NUMBER_OF_FILES - 1) < file_index)
    {
        BOARD_LOG("Open w failed - Invalid file index");
        return false;
    }

    if (NULL == p_path)
    {
        BOARD_LOG("Open w failed - NULL Pointer");
        return false;
    }

    self_.files[file_index] = SD.open(p_path, BOARD_SD_OPEN_WRITE_MODE);

    if (!(self_.files[file_index]))
    {
        BOARD_LOG("Open w failed - Invalid file");
        return false;
    }
    else
    {
        BOARD_LOG("Open w successfully");
        return true;
    }
}

bool board_sd_file_open_append_mode(int file_index, const char *p_path)
{
    if ((BOARD_SD_NUMBER_OF_FILES - 1) < file_index)
    {
        BOARD_LOG("Open a failed - Invalid file index");
        return false;
    }

    if (NULL == p_path)
    {
        BOARD_LOG("Open a failed - NULL Pointer");
        return false;
    }

    self_.files[file_index] = SD.open(p_path, BOARD_SD_OPEN_APPEND_MODE);

    if (!(self_.files[file_index]))
    {
        BOARD_LOG("Open a failed - Invalid file");
        return false;
    }
    else
    {
        BOARD_LOG("Open a successfully");
        return true;
    }
}

void board_sd_file_close(int file_index)
{

    if ((BOARD_SD_NUMBER_OF_FILES - 1) < file_index)
    {
        BOARD_LOG("Close failed - Invalid file index");
        return;
    }

    (self_.files[file_index]).close();

}

uint32_t board_sd_file_swrite(int file_index, const char *p_text)
{
    uint32_t len = 0;

    if ((BOARD_SD_NUMBER_OF_FILES - 1) < file_index)
    {
        BOARD_LOG("Invalid file index");
        return 0;
    }

    if (!(self_.files[file_index]))
    {
        BOARD_LOG("Invalid file");
        return 0;
    }

    len = (uint32_t) (self_.files[file_index]).print(p_text);

    if (len)
    {
        BOARD_LOG("File written");
    }
    else
    {
        BOARD_LOG("Write failed");
    }

    return (uint32_t) len;

}

uint32_t board_sd_file_sread(int file_index, char *p_buffer, uint32_t size)
{
    if ( NULL == p_buffer)
    {
        return 0;
    }

    if ((BOARD_SD_NUMBER_OF_FILES - 1) < file_index)
    {
        return 0;
    }

    return (uint32_t) (self_.files[file_index]).readBytes(p_buffer, size);

}

void board_sd_init()
{
    BOARD_LOG_TRACE_FUNCTION();
    self_.is_sd_mounted = false;

    board_sd_setup_();

}

#if 0 // deprecated
void board_sd_init(){
	board_sd_setup();
}
void board_sd_deinit(){

}
void board_sd_loop(){

}

void board_sd_setup(void){
	//mount SD
//    if(!SD.begin()){
//    	//error mount
//    	 BOARD_LOG("Card Mount Failed");
//     	self_.sd_mount_status = BOARD_SD_NOT_MOUNTED;
//        return;
//    }
//    else{
//    	BOARD_LOG("Card Mounted successfully");
//    	self_.sd_mount_status = BOARD_SD_MOUNTED_OK;
//    }
//
//	#if 0 //Not in use
//    //SD card type
//    board_sd_cardtype = SD.cardType();
//    //SD card size
//    board_sd_cardsize = SD.cardSize() / (1024 * 1024);
//	#endif
}

board_sd_mount_status_t board_sd_mount_status_get(void){

//		return self_.sd_mount_status;
}

board_file_t board_sd_file_open(const char * path, const char * mode){


//	File file = SD.open(path,mode);
//
//    if(!file){
//    	BOARD_LOG("Open failed - Invalid file");
//    }
//    else{
//    	BOARD_LOG("Open successfully");
//    }
//
//	return (board_file_t) file;
}

void board_sd_file_close( board_file_t file ){

//	file.close();

}

bool board_sd_file_exists(const char * path ){

//	  if (SD.exists(path))
//	  {
//		  BOARD_LOG("file exists");
//		  return true;
//	  }
//	  else
//	  {
//		  BOARD_LOG("file doesn't exist.");
//		  return false;
//	  }

}

void board_sd_file_delete(const char * path ){

//	SD.remove(path);
//
//	if(board_sd_file_exists(path) != false)
//	{
//		 BOARD_LOG("Error in delete");
//		//call error func
//	}
//	else{
//		 BOARD_LOG("file successfully deleted");
//	}

}



uint32_t board_sd_file_swrite( board_file_t board_file,const char * text){

//	File file;
//	uint32_t len=0;
//
//	file = (File) board_file;
//
//    if(!file){
//    	 BOARD_LOG("invalid file");
//        return 0;
//    }
//
//    len = (uint32_t)file.print(text);
//    if(len){
//    	 BOARD_LOG("File written");
//    } else {
//    	 BOARD_LOG("Write failed");
//    }
//
//    return (uint32_t) len;
}

uint32_t board_sd_file_sread( board_file_t file, char * buffer, uint8_t len_max ){

//	if( NULL == buffer){
//		//error
//		return 0;
//	}
//
//	return (uint32_t) file.readBytes(buffer, len_max);
}

uint32_t board_sd_file_sread_line(board_file_t file, char * buffer, uint32_t line_n){

//		uint32_t i=0;
//
//	    if(!file){
//	    	 BOARD_LOG("Invalid file");
//	        return 0;
//	    }
//
//	  for (i = 0; (i < line_n) && (file.available()!=0) ; ++i){
//	  	 strcpy(buffer,file.readStringUntil('\n').c_str());
//	  }
//
//	  if(i != line_n){
//		  //error --> less lines that line_n
//		  BOARD_LOG("less lines that line_n");
//		  return i;
//	  }
//
//	  return i;
}


bool board_sd_file_seek( board_file_t board_file , uint32_t position ){

//	File file;
//
//	file = (File) board_file;
//
//    if(!file){
//    	 BOARD_LOG("invalid file");
//        return 0;
//    }
//
//	return file.seek(position);

}

int board_sd_file_available( board_file_t board_file){

//	File file;
//	file = (File) board_file;
//    if(!file){
//    	 BOARD_LOG("invalid file");
//        return -1;
//    }
//
//	return file.available();

}
#endif

#if 0 //Not in use
board_sdcard_type_t board_sd_get_type(){
	return board_sd_cardtype;
}

int board_sd_get_size(){
	return board_sd_cardsize;
}

#endif

#if 0 //DEPRECATED -------------------------------------------------------------------------------------

int board_sd_read_line(const char * path, char * buffer, uint8_t line_n){

int i=0;
//
//    char aux[10];
//
//    if(!file){
//         BOARD_LOG("Failed to open file for reading");
//        return;
//    }
//
//    while(file.available()){
//     	//sprintf(aux,"%d",file.readString());
//     	strcat(buffer, aux);
//    }
//    file.close();
//    file.readStringUntil('c');

	File file = SD.open(path);

	    if(!file){
	    	 BOARD_LOG("Failed to open file for reading");
	        return -1;
	    }

	  for (i = 0; (i < line_n) && (file.available()!=0) ; ++i){
	  	 strcpy(buffer,file.readStringUntil('\n').c_str());
	  }

	  file.close();

	  return i;
}

int board_sd_read_regular_line(const char * path, char * buffer, uint8_t line_n, uint8_t line_size){

	File file = SD.open(path);

	    if(!file){
	         BOARD_LOG("Failed to open file for reading");
	        return -1;
	    }


	   if(file.seek((line_size+1) * (line_n-1)) == false){
		   return -1;
	   }

	  strcpy(buffer,file.readStringUntil('\n').c_str());

	  file.close();

	  return 0;
}


//static void board_sd_gets(const char * path, char * buffer){
//	//strcpy(buffer,file_global.readStringUntil('\0'));
//}
//
//
//static void board_sd_seek(uint32_t pos){
//	//file_global.seek(pos);
//}

void board_sd_overwrite(const char * path, const char * message){

    File file = SD.open(path, FILE_WRITE);

        if(!file){
         BOARD_LOG("Failed to open file for writing");
        return;
    }
    if(file.print(message)){
         BOARD_LOG("File written");
    } else {
         BOARD_LOG("Write failed");
    }

    file.close();
}

void board_sd_append(const char * path, const char * message){

    File file = SD.open(path, FILE_APPEND);

    if(!file){
         BOARD_LOG("Failed to open file for append");
        return;
    }
    if(file.print(message)){
         BOARD_LOG("File written");
    } else {
         BOARD_LOG("Write failed");
    }

    file.close();
}

#endif

/********************** end of file ******************************************/

