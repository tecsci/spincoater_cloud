/*
* <TECSCI Dip Coater  Technology for Science info@tecsci.com.ar>
* 			Copyright (C) <2022>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <https://www.gnu.org/licenses/>.
*
* @file		board_spi.c
* @date		14 jun. 2021
* @author	
*		-Martin Abel Gambarotta   magambarotta@gmail.com
* @version	v1.0.0
* @brief
*
* @{ Init Doxygen
*/

/********************** inclusions *******************************************/
#include "board_spi.hpp"
#include "SPI.h"
#include <stdio.h>
#include "driver/spi_master.h"

#include "hardware.hpp"
/********************** macros and definitions *******************************/

#define BOARD_ESP32_SPI_2_SCK 	HARDWARE_ESP32_SPI_2_SCK
#define BOARD_ESP32_SPI_2_MISO 	HARDWARE_ESP32_SPI_2_MISO
#define BOARD_ESP32_SPI_2_MOSI 	HARDWARE_ESP32_SPI_2_MOSI
#define BOARD_ESP32_SPI_2_CS 	HARDWARE_ESP32_SPI_2_CS

#define BOARD_ESP32_SPI_2_MAX_TRANSFER_SIZE 	HARDWARE_ESP32_SPI_2_MAX_TRANSFER_SIZE

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/


#if 1 == BOARD_SPI_ARDUINO_VERSION

board_spi_t p_spi = NULL;


static struct {

} self_;


#else
static struct {
	//Device
	spi_device_handle_t spi_device_1;
	//Config
	spi_bus_config_t spi_bus_config_1;
	spi_device_interface_config_t spi_device_config_1;

	//Device
	spi_device_handle_t spi_device_2;
	//Config
	spi_bus_config_t spi_bus_config_2;
	spi_device_interface_config_t spi_device_config_2;
} self_;
#endif

/********************** external data definition *****************************/

/********************** internal functions definition ************************/
#if 1 == BOARD_SPI_ARDUINO_VERSION
#else
static void board_spi_config_(board_spi_id_t spi_id) {

	switch (spi_id) {

	/* Reserved for  flash memory
	case BOARD_SPI_0:
	case BOARD_SPI_1:
	*/
	case BOARD_SPI_2:

		//BUS CONFIG

		self_.spi_bus_config_2.mosi_io_num = BOARD_ESP32_SPI_2_MOSI;	///< GPIO pin for Master Out Slave In (=spi_d) signal, or -1 if not used.
		self_.spi_bus_config_2.miso_io_num = BOARD_ESP32_SPI_2_MISO; ///< GPIO pin for Master In Slave Out (=spi_q) signal, or -1 if not used.
		self_.spi_bus_config_2.sclk_io_num = BOARD_ESP32_SPI_2_SCK; ///< GPIO pin for Spi CLocK signal, or -1 if not used.
		self_.spi_bus_config_2.quadwp_io_num = -1; ///< GPIO pin for WP (Write Protect) signal which is used as D2 in 4-bit communication modes, or -1 if not used.
		self_.spi_bus_config_2.quadhd_io_num = -1; ///< GPIO pin for HD (HolD) signal which is used as D3 in 4-bit communication modes, or -1 if not used.
//    self.spi_bus_config_2.max_transfer_sz = 	40;   ///< Maximum transfer size, in bytes. Defaults to 4094 if 0.
//    self.spi_bus_config_2.flags =;              ///< Abilities of bus to be checked by the driver. Or-ed value of ``SPICOMMON_BUSFLAG_*`` flags.
//    self.spi_bus_config_2.intr_flags =;

		//INTERFACE CONFIG

		//self.spi_device_config_2.command_bits =;           	//Default amount of bits in command phase (0-16), used when ``SPI_TRANS_VARIABLE_CMD`` is not used, otherwise ignored.
		//self.spi_device_config_2.address_bits =;           	//Default amount of bits in address phase (0-64), used when ``SPI_TRANS_VARIABLE_ADDR`` is not used, otherwise ignored.
		//self.spi_device_config_2.dummy_bits =;             	//Amount of dummy bits to insert between address and data phase
		self_.spi_device_config_2.mode = 1;                   	//SPI mode (0-3)
		//self.spi_device_config_2.duty_cycle_pos =;        	//Duty cycle of positive clock, in 1/256th increments (128 = 50%/50% duty). Setting this to 0 (=not setting it) is equivalent to setting this to 128.
		//self.spi_device_config_2.cs_ena_pretrans =;       	//Amount of SPI bit-cycles the cs should be activated before the transmission (0-16). This only works on half-duplex transactions.
		//self.spi_device_config_2.cs_ena_posttrans =;       	// Amount of SPI bit-cycles the cs should stay active after the transmission (0-16)
		self_.spi_device_config_2.clock_speed_hz = 100000; //< Clock speed, divisors of 80MHz, in Hz. See ``SPI_MASTER_FREQ_*``.
		//self.spi_device_config_2.input_delay_ns =;             	//< Maximum data valid time of slave. The time required between SCLK and MISO
		self_.spi_device_config_2.spics_io_num = BOARD_ESP32_SPI_2_CS; // CS GPIO pin for this device, or -1 if not used
		//self.spi_device_config_2.flags =;                 	//Bitwise OR of SPI_DEVICE_* flags
		self_.spi_device_config_2.queue_size = 1; //Transaction queue size. This sets how many transactions can be 'in the air' (queued using spi_device_queue_trans but not yet finished using spi_device_get_trans_result) at the same time
		// transaction_cb_t pre_cb;   				//Callback to be called before a transmission is started.
		// transaction_cb_t post_cb;  				//Callback to be called after a transmission has completed.

		spi_bus_initialize(HSPI_HOST, &self_.spi_bus_config_2, 0); //0 en que no use DMA TODO Revisar este tema
		spi_bus_add_device(HSPI_HOST, &self_.spi_device_config_2,
				&self_.spi_device_2);

		break;

	case BOARD_SPI_3:
		break;
	default:
		break;
	}

}
#endif
/********************** external functions definition ************************/
#if 1 == BOARD_SPI_ARDUINO_VERSION

board_spi_t board_spi_get_spi_pointer(void){
	return p_spi;
}


#else
void board_spi_read(void)
{

}
void board_spi_write_read(board_spi_id_t id, uint8_t *tx, uint8_t *rx,
		size_t length) {

	spi_transaction_t spi_transaction = { };

	spi_transaction.length = length; /*transaction length is in bits.*/
	spi_transaction.tx_buffer = tx;
	spi_transaction.rx_buffer = rx;

	switch (id) {

	/* Reserved for  flash memory
	 case BOARD_SPI_0:
	 case BOARD_SPI_1:
	 */
	case BOARD_SPI_2:

		//ret=spi_device_polling_transmit(spi_dev, &t);
		spi_device_transmit(self_.spi_device_2, &spi_transaction);
		break;

	case BOARD_SPI_3:
		spi_device_transmit(self_.spi_device_1, &spi_transaction);
		break;
	}

}
#endif

void board_spi_init(void)
{
#if 1 == BOARD_SPI_ARDUINO_VERSION

	p_spi = new SPIClass(2);

	p_spi->setFrequency(10000);

	p_spi->begin(BOARD_ESP32_SPI_2_SCK, BOARD_ESP32_SPI_2_MISO, BOARD_ESP32_SPI_2_MOSI);

	p_spi->setFrequency(10000);

#else
	  board_spi_config_(BOARD_SPI_2);
#endif
}



/********************** end of file ******************************************/
/** @}Final Doxygen */
