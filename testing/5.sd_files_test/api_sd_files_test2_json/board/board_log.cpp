/********************** inclusions *******************************************/

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "board_log.hpp"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

static bool g_enable_;

/********************** external data definition *****************************/

static char g_buffer_[BOARD_LOG_PRINT_BUFFER_LEN];

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_log_print(char* p_text)
{
    if(true == g_enable_)
    {
        board_serial_write(BOARD_LOG_SERIAL, p_text, strlen(p_text));
    }
}

void board_log_print_buffer(void)
{
    board_log_print(g_buffer_);
}

char* board_log_buffer(void)
{
    return g_buffer_;
}

void board_log_enable(bool value)
{
    g_enable_ = value;
}

void board_log_init(void)
{
    g_enable_ = true;
}

void board_log_loop(void)
{

}

/********************** end of file ******************************************/
