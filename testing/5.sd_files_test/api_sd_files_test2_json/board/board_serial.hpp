#ifndef BOARD_SERIAL_HPP_
#define BOARD_SERIAL_HPP_

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

/********************** macros ***********************************************/

//BOARD_SERIAL
#define BOARD_LAYOUT_SERIAL_0_ENABLE        (1)
#define BOARD_LAYOUT_SERIAL_0_PERIPH_ID     (0)
#define BOARD_LAYOUT_SERIAL_0_BAUDRATE      (115200)
#define BOARD_LAYOUT_SERIAL_0_RX            (3)
#define BOARD_LAYOUT_SERIAL_0_TX            (1)
#define BOARD_LAYOUT_SERIAL_0_CONFIG_MODE   (0x800001c) //8N1 --> 0x800001c

#define BOARD_LAYOUT_SERIAL_1_ENABLE        (0)
#define BOARD_LAYOUT_SERIAL_1_PERIPH_ID     (1)
#define BOARD_LAYOUT_SERIAL_1_BAUDRATE      (115200)
#define BOARD_LAYOUT_SERIAL_1_RX            (9)
#define BOARD_LAYOUT_SERIAL_1_TX            (10)
#define BOARD_LAYOUT_SERIAL_1_CONFIG_MODE   (0x800001c) //8N1 --> 0x800001c

#define BOARD_LAYOUT_SERIAL_2_ENABLE        (0)
#define BOARD_LAYOUT_SERIAL_2_PERIPH_ID     (2)
#define BOARD_LAYOUT_SERIAL_2_BAUDRATE      (115200)
#define BOARD_LAYOUT_SERIAL_2_RX            (16)
#define BOARD_LAYOUT_SERIAL_2_TX            (17)
#define BOARD_LAYOUT_SERIAL_2_CONFIG_MODE   (0x800001c) //8N1 --> 0x800001c

/********************** typedef **********************************************/

enum
{
    BOARD_SERIAL_0, BOARD_SERIAL_1, BOARD_SERIAL_2, BOARD_SERIAL__N,
};

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

uint32_t board_serial_write(int serial_id, void *p_buffer, uint32_t size);

uint32_t board_serial_read(int serial_id, void *p_buffer, uint32_t size_max);

void board_serial_init(void);

void board_serial_deinit(void);

void board_serial_loop(void);

#endif /* BOARD_SERIAL_HPP_ */
/********************** end of file ******************************************/

