#include "Arduino.h"
#include "board/board_spi.hpp"
#include "board/board_sd.hpp"
#include "board/board_serial.hpp"
#include "board/board_log.hpp"
#include "api/api_file.hpp"
#include "lib/json_lib.hpp"


#define MOD_TCP_COMMANDS_MAX_SZ 28

#define MOD_TCP_COMMANDS_COMMAND_BUFF_SZ MOD_TCP_COMMANDS_MAX_SZ
#define MOD_TCP_COMMANDS_DATA_BUFF_SZ 100

#define MOD_TCP_COMMANDS_MSG_BUFF_SZ MOD_TCP_COMMANDS_COMMAND_BUFF_SZ + MOD_TCP_COMMANDS_DATA_BUFF_SZ

#define MOD_TCP_COMMANDS_PARSING_CHAR ' '

char aux_buff[256];
arclibs_json_doc aux_doc;
arclibs_json_doc filter_doc;

static struct {

	char readed_command_buffer[MOD_TCP_COMMANDS_COMMAND_BUFF_SZ];
	char readed_data_buffer[MOD_TCP_COMMANDS_DATA_BUFF_SZ];
	char readed_msg_buff[MOD_TCP_COMMANDS_MSG_BUFF_SZ];

} self_;


bool mod_tcp_commands_parse_(){


	int i;

	strcpy(self_.readed_command_buffer,"");
	strcpy(self_.readed_data_buffer,"");

	for(i=0 ; i < MOD_TCP_COMMANDS_COMMAND_BUFF_SZ ; i++){

		if(MOD_TCP_COMMANDS_PARSING_CHAR == self_.readed_msg_buff[i]){
			strncpy(self_.readed_command_buffer,self_.readed_msg_buff,i);
			self_.readed_command_buffer[i] = '\0';
			//DEBUG_LOG("self_.readed_command_buffer: %s \n",self_.readed_command_buffer);
			break;
		}
	}



	if((MOD_TCP_COMMANDS_COMMAND_BUFF_SZ) == i){

		Serial.printf("(MOD_TCP_COMMANDS_COMMAND_BUFF_SZ-1) == i \n");
		strncpy(self_.readed_command_buffer,
				self_.readed_msg_buff,
				MOD_TCP_COMMANDS_COMMAND_BUFF_SZ);
		Serial.printf("self_.readed_command_buffer: %s \n",self_.readed_command_buffer);
		self_.readed_data_buffer[0]='\0';
		return true;
	}

	strncpy(self_.readed_data_buffer,
			self_.readed_msg_buff + (i+1)*sizeof(char),
			MOD_TCP_COMMANDS_DATA_BUFF_SZ-1);
	self_.readed_data_buffer[MOD_TCP_COMMANDS_DATA_BUFF_SZ-1]='\0';
	//DEBUG_LOG("self_.readed_data_buffer: %s \n", self_.readed_data_buffer);


	return true;

}

void setup()
{

	board_serial_init();
	board_log_init();

#if 0
	pinMode(19, OUTPUT);
	pinMode(0, OUTPUT);
	digitalWrite(19,HIGH);
	digitalWrite(0,HIGH);

	pinMode(18, OUTPUT);
	digitalWrite(18, LOW);
#endif
	delay(1000);

	board_spi_init();
	board_sd_init();


	if(true == board_sd_is_mounted()){
		Serial.println("board mounted!");
	}
	else{
		Serial.println("board not mounted!");
		while(1);
	}


	board_sd_list_root_files();

	api_file_init();

	Serial.println("OPENING IN READ MODE");
	if(false == api_file_open(API_FILE_WIFI_CREDENTIALS, "/wifi_credentials.txt", API_FILE_OPENMODE_READ)){
		Serial.println("error opening file");
		while(1);
	}


	Serial.println("READING 1 ");
	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, self_.readed_msg_buff, 512)){
		Serial.println(self_.readed_msg_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(self_.readed_msg_buff);
	}

	Serial.println("DSRLZ");
	mod_tcp_commands_parse_();

	Serial.println(self_.readed_command_buffer);
	Serial.println(self_.readed_data_buffer);

	Serial.println("READING 2");
	if(0 == api_file_sread_line(API_FILE_WIFI_CREDENTIALS, self_.readed_msg_buff, 512)){
		Serial.println(self_.readed_msg_buff);
		Serial.println("error reading file");
		while(1);
	}
	else{
		Serial.println(self_.readed_msg_buff);
	}

	Serial.println("DSRLZ 2");
	mod_tcp_commands_parse_();

	Serial.println(self_.readed_command_buffer);
	Serial.println(self_.readed_data_buffer);


#if 0
	filter_doc["ssid"] = true;
	filter_doc["pass"] = true;

	if(false == arclibs_json_deserialize_with_filter(aux_doc, aux_buff,filter_doc)){
		Serial.println("error deserialize with filter");
	}
	else{

		const char* ssid = aux_doc["ssid"];
		const char* pass = aux_doc["pass"];

		Serial.println(ssid);
		Serial.println(pass);

	}
#endif

	// Print the result
//	serializeJsonPretty(aux_doc, Serial);



	Serial.println("CLOSE");

	api_file_close(API_FILE_WIFI_CREDENTIALS);


}

// The loop function is called in an endless loop
void loop()
{
//Add your repeated code here
}
