#include "Arduino.h"
#include "board/board_wifi.hpp"
#include "board/board_awsmqtt.hpp"
#include "api/api_awstelemetry.hpp"
#include "app_data/app_data.hpp"
#include <MQTTClient.h>
#include <ArduinoJson.h>


const char M_WIFI_SSID[] = "FUNINTEC-CTZ"; //"Fibertel Heredia  2.4GHz";
const char M_WIFI_PASSWORD[] = "UnsamFun2019";//"0123456789";

char main_buff[512];


void test_log_handler(const char* data){
	Serial.printf("log handler: %s \n", data);
}


void setup()
{

	Serial.begin(115200);

	//BOARD INIT
	board_wifi_init();


	board_wifi_connect(M_WIFI_SSID, M_WIFI_PASSWORD);

	while (true == board_wifi_is_connected()){
		delay(500);
		Serial.print(".");
	}

	board_awsmqtt_init();
	board_awsmqtt_config();

	board_awsmqtt_subscribe_set_handler(BOARD_AWSMQTTSUB_LOG_TAG, test_log_handler);

	delay(1000);

	//API
	api_awstelemetry_init();
	//APP
	app_data_init();

	delay(1000);
}


void loop()
{

  board_awsmqtt_loop();
  delay(1000);
}
