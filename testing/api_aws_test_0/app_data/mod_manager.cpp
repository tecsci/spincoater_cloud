/********************** inclusions *******************************************/


#include "mod_manager.hpp"

#include "../config/esysconfig.hpp"
#include "../api/api_awstelemetry.hpp"
#include "../board/board_awsmqtt.hpp"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

static struct{

	api_awstelemetry_data2timestream_t ts_data;
	api_awstelemetry_data2firebase_t fb_data;

}self_;

/********************** external functions definition ************************/


void mod_manager_init(void) {

	xTaskCreate(mod_manager_task_loop,
			"mod manager task",	ESYSCONFIG_MOD_MANAGER_TASK_SIZE,
			NULL,ESYSCONFIG_MOD_MANAGER_TASK_PRIORITY,NULL);
}

void mod_manager_deinit(void) {

}

void mod_manager_task_loop(void *pvParameters) {


	self_.ts_data.temp=0;
	self_.ts_data.hum=20;
	self_.ts_data.stime=0;

	self_.fb_data.temp1=0;
	self_.fb_data.hum1=20;
	self_.fb_data.temp2=15;
	self_.fb_data.hum2=60;
	self_.fb_data.stime=0;

	for (;;) {

		api_awstelemetry_send2timestream(self_.ts_data);

		self_.ts_data.temp++;
		self_.ts_data.hum+=3;
		self_.ts_data.stime+=10;

		if(self_.ts_data.temp > 1000){
			self_.ts_data.temp=0;
		}

		if(self_.ts_data.hum > 100){
			self_.ts_data.hum=0;
		}


		api_awstelemetry_send2firestore(self_.fb_data);

		self_.fb_data.temp1++;
		self_.fb_data.hum1+=3;
		self_.fb_data.temp2+=7;
		self_.fb_data.hum2+=6;
		self_.fb_data.stime+=10;

		if(self_.fb_data.temp1 > 1000){
			self_.fb_data.temp1=0;
		}

		if(self_.fb_data.temp2 > 700){
			self_.fb_data.temp2=0;
		}

		if(self_.fb_data.hum1 > 100){
			self_.fb_data.hum1=0;
		}

		if(self_.fb_data.hum2 > 100){
			self_.fb_data.hum2=0;
		}

		vTaskDelay(ESYSCONFIG_MOD_MANAGER_TASK_PERIOD);
	}
}



/********************** end of file ******************************************/
