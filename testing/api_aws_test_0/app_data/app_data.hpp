#ifndef APP_DATA_HPP_
#define APP_DATA_HPP_

/********************** inclusions *******************************************/

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void app_data_init(void);
void app_data_deinit(void);
void app_data_task_loop(void *pvParameters);

#endif /* APP_DATA_HPP_ */
/********************** end of file ******************************************/
