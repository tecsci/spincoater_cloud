#ifndef MOD_MANAGER_HPP_
#define MOD_MANAGER_HPP_

/********************** inclusions *******************************************/

/********************** macros ***********************************************/

/********************** typedef **********************************************/

/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void mod_manager_init(void);
void mod_manager_deinit(void);
void mod_manager_task_loop(void *pvParameters);

#endif /* MOD_MANAGER_HPP_ */
/********************** end of file ******************************************/
