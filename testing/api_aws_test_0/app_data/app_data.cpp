/********************** inclusions *******************************************/

#include "app_data.hpp"
#include "mod_manager.hpp"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/
/********************** external functions definition ************************/


void app_data_init(void) {

	mod_manager_init();
}

void app_data_deinit(void) {

}

void app_data_task_loop(void *pvParameters) {

}



/********************** end of file ******************************************/
