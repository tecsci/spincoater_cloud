/********************** inclusions *******************************************/

#include "api_awstelemetry.hpp"
#include "../lib/json_lib.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal data definition *****************************/

static struct{

	char aux_buff1[API_AWSTELEMETRY_BUFF_SZ];
	char aux_buff2[API_AWSTELEMETRY_BUFF_SZ];
	arclibs_json_doc aux_json1;
	arclibs_json_doc aux_json2;

}self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void api_awstelemetry_send2timestream(api_awstelemetry_data2timestream_t data){


	strcpy(self_.aux_buff1, "");


	self_.aux_json1["id"]=API_AWSTELEMETRY_DEVICE_ID;
	self_.aux_json1["loc"]=API_AWSTELEMETRY_LOCATION;
	self_.aux_json1["temp"]=data.temp;
	self_.aux_json1["hum"]=data.hum;
	self_.aux_json1["stime"]=data.stime;
	self_.aux_json1["log"]="nan";


	arclibs_json_serialize(self_.aux_buff1,self_.aux_json1, API_AWSTELEMETRY_BUFF_SZ);
	//strcpy(aux_buff, "{\"id\":\"dev0\",\"loc\":\"x\",\"temp\":22,\"hum\":85,\"stime\":85,\"log\":\"testing\"}");

	board_awsmqtt_publish(BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM_TAG,self_.aux_buff1);

}
void api_awstelemetry_send2firestore(api_awstelemetry_data2firebase_t data){

	/*FIREBASE payload:
	 *{
		"id": "aws_test",
		"stime": "10",
		"temp1": "22",
		"hum1": "23",
		"temp2": "24",
		"hum2": "25"
		"log":"nan"
	}
	 */

	strcpy(self_.aux_buff2, "");


	self_.aux_json2["id"]=API_AWSTELEMETRY_DEVICE_ID;
	self_.aux_json2["temp1"]=data.temp1;
	self_.aux_json2["hum1"]=data.hum1;
	self_.aux_json2["temp2"]=data.temp2;
	self_.aux_json2["hum2"]=data.hum2;
	self_.aux_json2["stime"]=data.stime;
	self_.aux_json2["log"]="nan";


	arclibs_json_serialize(self_.aux_buff2,self_.aux_json2, API_AWSTELEMETRY_BUFF_SZ);
	//strcpy(aux_buff, "{\"id\":\"dev0\",\"loc\":\"x\",\"temp\":22,\"hum\":85,\"stime\":85,\"log\":\"testing\"}");

	board_awsmqtt_publish(BOARD_AWSMQTTPUB_TELEMETRY2FIRESTORE_TAG,self_.aux_buff2);


}

void api_awstelemetry_init(void)
{

}


/********************** end of file ******************************************/

