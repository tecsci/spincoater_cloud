#ifndef API_AWSTELEMETRY_HPP_
#define API_AWSTELEMETRY_HPP_

/********************** inclusions *******************************************/

#include "../board/board_awsmqtt.hpp"
#include "../lib/json_lib.hpp"
#include <stdlib.h>
#include <stdint.h>



/********************** macros ***********************************************/

#define  API_AWSTELEMETRY_DEVICE_ID "dev_test"
#define  API_AWSTELEMETRY_LOCATION "funintec"

#define API_AWSTELEMETRY_BUFF_SZ 512
#define  API_AWSTELEMETRY_LOG_MAX_SZ 64

/********************** typedef **********************************************/


/*TIMESTEREAM payload:
	{"id":"dev0",
	"loc":"x",
	"temp": 22,
	"hum":80,
	"stime":1000,
	"log":"nan"
 }
 */
typedef struct{

	int32_t temp;
	int32_t hum;
	int32_t stime;
	char log[API_AWSTELEMETRY_LOG_MAX_SZ];


}api_awstelemetry_data2timestream_t;


/*FIREBASE payload:
 *{
	"id": "aws_test",
	"stime": "10",
	"temp1": "22",
	"hum1": "23",
	"temp2": "24",
	"hum2": "25"
	"log":"nan"
}
 */
typedef struct{

	int32_t temp1;
	int32_t hum1;
	int32_t temp2;
	int32_t hum2;
	int32_t stime;
	char log[API_AWSTELEMETRY_LOG_MAX_SZ];

}api_awstelemetry_data2firebase_t;


/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void api_awstelemetry_send2timestream(api_awstelemetry_data2timestream_t data);
void api_awstelemetry_send2firestore(api_awstelemetry_data2firebase_t data);
void api_awstelemetry_init(void);

#endif /* API_AWSTELEMETRY_HPP_ */
/********************** end of file ******************************************/

