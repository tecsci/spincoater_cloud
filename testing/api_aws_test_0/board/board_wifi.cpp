/*
 * Copyright (c) 2021 Sebastian Bedin <sebabedin@gmail.com>.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from
 *    this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
 * IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 *
 * @file   : board_wifi.cpp
 * @date   : Feb 17, 2021
 * @author : Sebastian Bedin <sebabedin@gmail.com>
 * @version	v1.0.0
 */

/********************** inclusions *******************************************/

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include "Arduino.h"
#include <WiFi.h>
#include "board_wifi.hpp"
//#include "board_log.hpp"

/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

/********************** internal functions declaration ***********************/

/********************** internal data definition *****************************/

static struct
{
    bool enable;
} self_;

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

/********************** external functions definition ************************/

void board_wifi_ip(uint8_t *p_ip)
{
    IPAddress wifi_ip = WiFi.localIP();
    for(int idx = 0; idx < 4; ++idx)
    {
        p_ip[idx] = wifi_ip[idx];
    }
}

void board_wifi_connect(const char *p_ssid, const char *p_password)
{
	  self_.enable = true;
	  WiFi.begin(p_ssid, p_password);
}

void board_wifi_disconnect(void)
{
    self_.enable = false;
    WiFi.disconnect(true);
}

bool board_wifi_is_connected(void)
{
    return WiFi.isConnected();
}

void board_wifi_init(void)
{
    //BOARD_LOG_TRACE_FUNCTION();
    WiFi.mode(WIFI_STA);
    self_.enable = false;
}

void board_wifi_deinit(void)
{
    //BOARD_LOG_TRACE_FUNCTION();
    WiFi.disconnect(true);
}

void board_wifi_reconnect(void)
{
    WiFi.reconnect();
}

void board_wifi_loop(void)
{
#if 0
	static int cnt = 0;

    cnt++;
    if(50 < cnt)
    {
        cnt = 0;
        if((true == self_.enable)
                && (false == board_wifi_is_connected()))
        {
            WiFi.reconnect();
        }
    }
#endif
}

#if 0
void board_wifi_disconnected_event_(WiFiEvent_t event, WiFiEventInfo_t info){
	//info.disconnected.reason

	if(true == self_.first_connect){
		WiFi.begin(self_.ssid_reconnect, self_.pass_reconnect);
	}


}


void board_wifi_auto_reconnect_enable(bool enable)
{

	if(true == enable){

		if(true == self_.first_connect){
			WiFi.onEvent(board_wifi_disconnected_event_ , SYSTEM_EVENT_STA_DISCONNECTED);
		}
		else{
			BOARD_LOG("ERROR: Wifi auto reconnect event requires a previous board_wifi_connect");
		}
	}

}

#endif


/********************** end of file ******************************************/
