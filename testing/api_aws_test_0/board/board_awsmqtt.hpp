/*
* Copyright (c) 2020 Martin Gambarotta <magambarotta@gmail.com>.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from
*    this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* @file	board_awsmqtt.hpp
* @date	2022-24-08
* @author:
*  - Mancini Lucas <mancinilucas95@gmail.com>
* @version	v1
*/

#ifndef BOARD_AWSMQTT_HPP_
#define BOARD_AWSMQTT_HPP_


/********************** inclusions *******************************************/

#include "Arduino.h"
#include "../secrets/aws_iotcore_secrets.h"

/********************** macros ***********************************************/

#define BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM_STR "devicepub/ts/1"
#define BOARD_AWSMQTTPUB_TELEMETRY2FIRESTORE_STR "devicepub/fs/1"
#define BOARD_AWSMQTTPUB_METADATA_STR "devicepub/metadata/1"

#define BOARD_AWSMQTTSUB_RESET_STR "devicesub/reset/1"
#define BOARD_AWSMQTTSUB_LOG_STR "devicesub/log/1"


//#define AWS_IOT_PUBLISH_TOPIC   "device/29/data" //"esp32/pub"
//#define AWS_IOT_SUBSCRIBE_TOPIC "device29/sub"//"esp32/sub"

/********************** typedef **********************************************/

typedef struct{

	char name[128];

}board_awsmqtt_pubtopic_t;

typedef struct{

	char name[128];
	void (*handler)(const char*);

}board_awsmqtt_subtopic_t;


typedef enum{
	BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM_TAG=0,
	BOARD_AWSMQTTPUB_TELEMETRY2FIRESTORE_TAG,
	BOARD_AWSMQTTPUB_METADATA_TAG,
	BOARD_AWSMQTTPUB__N
}board_awsmqtt_pubtopic_tag_t;

typedef enum{
	BOARD_AWSMQTTSUB_RESET_TAG=0,
	BOARD_AWSMQTTSUB_LOG_TAG,
	BOARD_AWSMQTTSUB__N
}board_awsmqtt_subtopic_tag_t;


/********************** external data declaration ****************************/

/********************** external functions declaration ***********************/

void board_awsmqtt_config(void);


void board_awsmqtt_publish(board_awsmqtt_pubtopic_tag_t tag, const char * buff);
void board_awsmqtt_subscribe_set_handler(board_awsmqtt_subtopic_tag_t tag, void (*handler)(const char*));

bool board_awsmqtt_is_connected();

void board_awsmqtt_init(void);
void board_awsmqtt_deinit(void);
void board_awsmqtt_loop(void);

void board_awsmqtt_dummy_handler(const char* data);

#if 0
void board_mqtt_begin(const char * host_name, int port, WiFiClientSecure wifi_sec);
bool board_mqtt_connect(const char * cliente_id);
void board_mqtt_disconnect(void);
bool board_mqtt_is_connected();

void board_mqtt_publish(const char * topic, const char * msg);
void board_mqtt_subscribe(const char * topic);
bool board_mqtt_client_loop(void);
//WIP
void board_mqtt_re_connect(void);
void board_mqtt_stop(void);
bool board_mqtt_new_msg_check(char * msg);

#endif

#endif /* BOARD_AWSMQTT_HPP_ */
/********************** end of file ******************************************/
