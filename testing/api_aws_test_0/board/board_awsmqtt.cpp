/*
* Copyright (c) 2020 Martin Gambarotta <magambarotta@gmail.com>.
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright
*    notice, this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the
*    documentation and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its
*    contributors may be used to endorse or promote products derived from
*    this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
* "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
* LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
* FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
* COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
* SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
* HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
* STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING
* IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*
* @file	board_awsmqtt.cpp
* @date	2022-24-08
* @author:
*  - Mancini Lucas <mancinilucas95@gmail.com>
* @version	v1
*/

/********************** inclusions *******************************************/

#include "board_awsmqtt.hpp"
//#include <ArduinoJson.h>
#include <MQTTClient.h>
#include <WiFiClientSecure.h>
#include "Arduino.h"


/********************** macros and definitions *******************************/

/********************** internal data declaration ****************************/

MQTTClient client = MQTTClient(256);
WiFiClientSecure net = WiFiClientSecure();

static struct {

	board_awsmqtt_subtopic_t sub_topics[BOARD_AWSMQTTSUB__N];
	board_awsmqtt_pubtopic_t pub_topics[BOARD_AWSMQTTPUB__N];

}self_;

/********************** internal data definition *****************************/

/********************** external data definition *****************************/

/********************** internal functions definition ************************/

void simple_msg_handler_(String &topic, String &payload) {

	for(int i=0;i<BOARD_AWSMQTTSUB__N;i++){
		if(0== strcasecmp(topic.c_str(),self_.sub_topics[i].name)){
			Serial.printf("encontrado el topic: fue el %d \n",i);
			//ACA LLAMAR A LOS HANDLERS O ALGO ASI
			self_.sub_topics[i].handler(payload.c_str());
		}
	}

  Serial.println("incoming: " + topic + " - " + payload);

//  StaticJsonDocument<200> doc;
//  deserializeJson(doc, payload);
//  const char* message = doc["message"];
}

static void advanced_msg_handler_(char topic[], char bytes[], int length){
	return;
}


/********************** external functions definition ************************/

#if 0
void board_mqtt_begin(const char * host_name, int port, WiFiClientSecure wifi_sec){

	//client.begin(host_name, port, wifi_sec);

	client.begin(AWS_IOT_ENDPOINT, 8883, wifi_sec);

	// Create a message handler
	client.onMessage(simple_msg_handler_);

#if 0 //FOR ANDAVANCED HANDLER WIP TODO
	client.onMessageAdvanced(advanced_msg_handler_);
#endif
}

#if 0
void board_mqtt_begin(const char * host_name, int port, board_wifisecurity_config_t wifi_sec){

	client.begin(host_name, port, wifi_sec);

	// Create a message handler
	client.onMessage(simple_msg_handler_);

#if 0 //FOR ANDAVANCED HANDLER WIP TODO
	client.onMessageAdvanced(advanced_msg_handler_);
#endif
}
#endif

bool board_mqtt_connect(const char * cliente_id){

	return client.connect(THINGNAME);

}

bool board_mqtt_is_connected(){
	if(!client.connected()){
		return false;
	}

	return true;
}

void  board_mqtt_publish(const char * topic, const char * msg){
	client.publish(topic, msg);
}

void  board_mqtt_subscribe(const char * topic){
	client.subscribe(topic);
}


void board_mqtt_disconnect(void){

	client.disconnect();

}

void board_mqtt_re_connect(void) {

#if 0
	//BOARD_LOG("Attempting MQTT reconnection...");
     if (client.connect(self_.mqtt_client_id, BOARD_MQTT_USER, BOARD_MQTT_PASSWORD)) {
        client.subscribe(BOARD_MQTT_TOPIC_SUB);
        BOARD_LOG("MQTT client connected");

    } else {

    	BOARD_LOG("Disconnect MQTT Server");

    }
#endif
}

bool board_mqtt_client_loop(void) {

	client.loop();

}


void board_mqtt_stop(void) {

}

bool board_mqtt_new_msg_check(char * msg){

#if 0
	if (true == self_.read_flag)
	{
		strncpy(msg, self_.msg,BOARD_MQTT_MSG_SIZE);
		self_.read_flag = false;

		return true;

	}

	return false;
#endif
}

#endif

void board_awsmqtt_config(void){

	//WIFI SECURITY
	// Configure WiFiClientSecure to use the AWS IoT device credentials
	net.setCACert(BOARD_AWSMQTT_CERT_CA);
	net.setCertificate(BOARD_AWSMQTT_CERT_CRT);
	net.setPrivateKey(BOARD_AWSMQTT_CERT_PRIVATE);

	client.begin(BOARD_AWSMQTT_IOTCORE_ENDPOINT, 8883, net);

	// Create a message handler
	client.onMessage(simple_msg_handler_);

	Serial.print("Connecting to AWS IOT");

	while (!client.connect(BOARD_AWSMQTT_IOTCORE_THINGNAME)) {
		Serial.print(".");
		delay(100);
	}

	if(!client.connected()){
		Serial.println("AWS IoT Timeout!");
		return;
	}
	else{
		Serial.println("AWS IoT Connected!");
	}

	// Subscribe to a topic
	//client.subscribe(AWS_IOT_SUBSCRIBE_TOPIC);

	for(int i = 0; i < BOARD_AWSMQTTSUB__N;i++){
		client.subscribe(self_.sub_topics[i].name);
	}

	Serial.println("AWS IoT Connected!");


}

void board_awsmqtt_publish(board_awsmqtt_pubtopic_tag_t tag, const char * buff)
{

	client.publish(self_.pub_topics[tag].name, buff);

  //client.publish(AWS_IOT_PUBLISH_TOPIC, buff);
}


void board_awsmqtt_subscribe_set_handler(board_awsmqtt_subtopic_tag_t tag, void (*handler)(const char*)){

	self_.sub_topics[tag].handler=handler;
}

bool board_awsmqtt_is_connected(){

	return client.connected();
}




void board_awsmqtt_init(void){


	//Set PUB Topics
	strcpy(self_.pub_topics[BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM_TAG].name, BOARD_AWSMQTTPUB_TELEMETRY2TIMESTREAM_STR);
	strcpy(self_.pub_topics[BOARD_AWSMQTTPUB_TELEMETRY2FIRESTORE_TAG].name, BOARD_AWSMQTTPUB_TELEMETRY2FIRESTORE_STR);
	strcpy(self_.pub_topics[BOARD_AWSMQTTPUB_METADATA_TAG].name,BOARD_AWSMQTTPUB_METADATA_STR);


	//Set SUB Topics
	strcpy(self_.sub_topics[BOARD_AWSMQTTSUB_LOG_TAG].name,BOARD_AWSMQTTSUB_LOG_STR);
	self_.sub_topics[BOARD_AWSMQTTSUB_LOG_TAG].handler=board_awsmqtt_dummy_handler;

	strcpy(self_.sub_topics[BOARD_AWSMQTTSUB_RESET_TAG].name,BOARD_AWSMQTTSUB_RESET_STR);
	self_.sub_topics[BOARD_AWSMQTTSUB_RESET_TAG].handler=board_awsmqtt_dummy_handler;

}

void board_awsmqtt_deinit(void){

	//board_mqtt_stop();

}

void board_awsmqtt_loop(void) {
	client.loop();
}

void board_awsmqtt_dummy_handler(const char* data){
	Serial.printf("dummy_handler: %s \n", data);
}


/********************** end of file ******************************************/
